<div class="page-wrapper">
  <div class="content container-fluid">
    <div class="page-header">
      <div class="row">
        <div class="col-sm-12">
          <h3 class="page-title">Welcome <?=$this->session->userdata('name')?>!</h3>
          <ul class="breadcrumb">
            <li class="breadcrumb-item active">Dashboard</li>
          </ul>
        </div>
      </div>
      <!-- <div class="row">
        <?php if($this->session->userdata('user_type')==1 || $this->session->userdata('user_type')==2){?>
        <div class="col-xl-3 col-sm-6 col-12">
          <div class="card">
            <div class="card-body">
              <div class="dash-widget-header">
                <span class="dash-widget-icon text-primary border-primary">
                  <i class="fe fe-users"></i>
                </span>
                <div class="dash-count">
                  <h3><?=count($admin)?></h3>
                </div>
              </div>
              <div class="dash-widget-info">
                <h6 class="text-muted">Admin</h6>
                <div class="progress progress-sm">
                  <div class="progress-bar bg-primary w-50"></div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-xl-3 col-sm-6 col-12">
          <div class="card">
            <div class="card-body">
              <div class="dash-widget-header">
                <span class="dash-widget-icon text-primary border-primary">
                  <i class="fe fe-users"></i>
                </span>
                <div class="dash-count">
                  <h3><?=count($user)?></h3>
                </div>
              </div>
              <div class="dash-widget-info">
                <h6 class="text-muted">User</h6>
                <div class="progress progress-sm">
                  <div class="progress-bar bg-primary w-50"></div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-xl-3 col-sm-6 col-12">
          <div class="card">
            <div class="card-body">
              <div class="dash-widget-header">
                <span class="dash-widget-icon text-primary border-primary">
                  <i class="fe fe-users"></i>
                </span>
                <div class="dash-count">
                  <h3><?=count($client)?></h3>
                </div>
              </div>
              <div class="dash-widget-info">
                <h6 class="text-muted">Client</h6>
                <div class="progress progress-sm">
                  <div class="progress-bar bg-primary w-50"></div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-xl-3 col-sm-6 col-12">
          <div class="card">
            <div class="card-body">
              <div class="dash-widget-header">
                <span class="dash-widget-icon text-success">
                  <i class="fa fa-project-diagram"></i>
                </span>
                <div class="dash-count">
                  <h3><?=count($projects)?></h3>
                </div>
              </div>
              <div class="dash-widget-info">
                <h6 class="text-muted">Projects</h6>
                <div class="progress progress-sm">
                  <div class="progress-bar bg-success w-50"></div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-xl-3 col-sm-6 col-12">
          <div class="card">
            <div class="card-body">
              <div class="dash-widget-header">
                <span class="dash-widget-icon text-danger border-danger">
                  <i class="fa-solid fa-tower-observation"></i>
                </span>
                <div class="dash-count">
                  <h3><?=count($work_performed)?></h3>
                </div>
              </div>
              <div class="dash-widget-info">
                <h6 class="text-muted">Defect</h6>
                <div class="progress progress-sm">
                  <div class="progress-bar bg-danger w-50"></div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-xl-3 col-sm-6 col-12">
          <div class="card">
            <div class="card-body">
              <div class="dash-widget-header">
                <span class="dash-widget-icon text-warning border-warning">
                  <i class="fa fa-road"></i>
                </span>
                <div class="dash-count">
                  <h3><?=count($road_inspection)?></h3>
                </div>
              </div>
              <div class="dash-widget-info">
                <h6 class="text-muted">Road Inspection</h6>
                <div class="progress progress-sm">
                  <div class="progress-bar bg-warning w-50"></div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <?php } if($this->session->userdata('user_type')==3){?>
        <div class="col-xl-3 col-sm-6 col-12">
          <div class="card">
            <div class="card-body">
              <div class="dash-widget-header">
                <span class="dash-widget-icon text-warning border-warning">
                  <i class="fe fe-road"></i>
                </span>
                <div class="dash-count">
                  <h3><?=count($road_inspection)?></h3>
                </div>
              </div>
              <div class="dash-widget-info">
                <h6 class="text-muted">Road Inspection</h6>
                <div class="progress progress-sm">
                  <div class="progress-bar bg-warning w-50"></div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <?php } ?>

      </div> -->
      <div class="row">

        <?php foreach($projects as $project){?>
        <div class="col-xl-6 col-sm-6 col-12">
            <div class="card">
                <h5 class="px-3 pt-3"><?=$project->name?></h5>
                <div id="piechart<?=$project->id?>" style="width: 100%; height: 500px;"></div>
            </div>
        </div>
     
        <?php } ?>

      </div>
    </div>
  </div>
</div>
</div>

<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<?php foreach($projects as $project){
  $this->db->select('road_inspetion_maintenance.*,work_perform.name as defectName');
  $this->db->join('work_perform','work_perform.id=road_inspetion_maintenance.work_perform','left' );
  $this->db->where('road_inspetion_maintenance.projectID',$project->id);
  $this->db->group_by('road_inspetion_maintenance.work_perform');
  $road = $this->db->get('road_inspetion_maintenance')->result();
  ?>
<script type="text/javascript">
google.charts.load('current', {
  'packages': ['corechart']
});
google.charts.setOnLoadCallback(drawChart);

function drawChart() {

  var data = google.visualization.arrayToDataTable([
    ['Road Inspection', 'Work On'],
   <?php 
   if(count($road)>0){ 
    // echo "<pre>";
    // print_r($project->id.'='.$data->work_perform);
    foreach($road as $key=>$data){
      $this->db->select('road_inspetion_maintenance.*,work_perform.name as defectName');
    $this->db->join('road_inspetion_maintenance','work_perform.id=road_inspetion_maintenance.work_perform','left' );
    $this->db->where('road_inspetion_maintenance.projectID',$project->id);
    $this->db->where('road_inspetion_maintenance.work_perform',$data->work_perform);
    $roads = $this->db->get('work_perform')->result();
  //echo $this->db->last_query();
      ?>
      ['<?=$data->defectName?>', <?=count($roads)?>],
      <?php
    }
   }else{
    ?>
   ['Road Inspection Not Start',1]
    <?php
   }
    ?>
 
  ]);
  
  var options = {
 
    title: 'Tasks Completed',
    
    pieSliceText: 'value-and-percentage',
    sliceVisibilityThreshold :0,
    fontSize: 17,
    legend: {
      position: 'value'
    },
  };

//   var options = {
//     legend: 'none',
//      pieSliceText: 'label-and-percentage'
   
//   };

  var chart = new google.visualization.PieChart(document.getElementById('piechart<?=$project->id?>'));

  chart.draw(data, options);
}
</script>
<?php } ?>