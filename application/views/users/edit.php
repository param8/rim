<div class="main-wrapper">
  <div class="page-wrapper">
    <div class="content container-fluid">
      <div class="page-header">
        <div class="row">
          <div class="col-md-12">
            <div class="card">
              <div class="card-header">
                <h4 class="card-title">Personal Information</h4>
              </div>
              <div class="card-body">
                <form action="<?=base_url('User/update')?>" id="updateForm" method="POST" enctype="multipart/form-data">
                  <div class="row">
                    <div class="col-xl-6">
                      <div class="form-group row">
                        <label class="col-lg-3 col-form-label">Name</label>
                        <div class="col-lg-9">
                          <input type="text" class="form-control" id="name" name="name" value="<?=$user->name?>">
                        </div>
                      </div>
                      <div class="form-group row">
                        <label class="col-lg-3 col-form-label">Email</label>
                        <div class="col-lg-9">
                          <input type="text" class="form-control" value="<?=$user->email?>" readonly>
                        </div>
                      </div>

                    </div>
                    <div class="col-xl-6">
                      <div class="form-group row">
                        <label class="col-lg-3 col-form-label">Phone</label>
                        <div class="col-lg-9">
                          <input type="text" class="form-control" value="<?=$user->phone?>" readonly>
                        </div>
                      </div>
                      <div class="form-group row">
                        <label class="col-lg-3 col-form-label">Profile Pic</label>
                        <div class="col-lg-9">
                          <input type="file" class="form-control" name="profile_pic" id="profile_pic" accept="image/*">
                          <img src="<?=base_url($user->profile_pic);?>" height="50" width="50">
                        </div>
                      </div>

                    </div>
                  </div>
                  <h4 class="card-title">Address</h4>
                  <div class="row">

                    <div class="col-xl-6">
                      <div class="form-group row">
                        <label class="col-lg-3 col-form-label">State</label>
                        <div class="col-lg-9">

                          <select class="select" name="state" id="state" onchange="getCity(this.value)">
                            <option>Select state</option>
                            <?php 
                              foreach($states as $state){
                              ?>
                            <option value="<?=$state->id?>" <?=$user->state==$state->id ? 'selected' : ''?>>
                              <?=$state->name?></option>
                            <?php }?>
                          </select>
                        </div>
                      </div>
                    </div>
                    <div class="col-xl-6">
                      <div class="form-group row">
                        <label class="col-lg-3 col-form-label">City</label>
                        <div class="col-lg-9">
                          <!-- <input type="text" class="form-control" value="<?//=$staff->cityName?>"> -->
                          <select class="select" name="city" id="city">
                          </select>
                        </div>
                      </div>
                    </div>
                    <div class="col-xl-12">
                      <div class="form-group row">
                        <label class="col-lg-3 col-form-label">Address</label>
                        <textarea type="text" class="form-control" id="address" name="address"
                          value=""><?=$user->address?></textarea>
                      </div>

                    </div>

                  </div>
              </div>

              <div class="col-md-12 col-lg-12 col-xl-12">
                <div class="card">
                  <div class="card-header">
                    <h4 class="card-title">Provide Permission</h4>
                  </div>
                  <div class="card-body">
                    <div class="table-responsive">
                      <table class=" table table-hover table-center mb-0">
                        <thead>
                          <tr>
                            <th>Menu</th>
                            <th>View</th>
                            <th>Add</th>
                            <th>Edit</th>
                            <th>Delete</th>
                            <th>Upload</th>
                            <th>Export</th>
                          </tr>
                        </thead>
                        <tbody>
                          <?php
                    // print_r($menus);
                    foreach($menus as $menu){
                      $menuID = $menu->id;
                      $permission = $user->permission;
                      $permission = json_decode($permission)->$menuID;
                      if($menu->child_name!='Dashboard'){
                      ?>
                          <tr>
                            <td><?=$menu->child_name?></td>
                            <td><input type="checkbox" name="view[<?=$menu->id?>]"
                                <?=in_array('View',$permission) ? 'checked' : ''?> value="View"></td>
                            <td><input type="checkbox" name="add[<?=$menu->id?>]"
                                <?=in_array('Add',$permission) ? 'checked' : ''?> value="Add"></td>
                            <td><input type="checkbox" name="edit[<?=$menu->id?>]"
                                <?=in_array('Edit',$permission) ? 'checked' : ''?> value="Edit"></td>
                            <td><input type="checkbox" name="delete[<?=$menu->id?>]"
                                <?=in_array('Delete',$permission) ? 'checked' : ''?> value="Delete"></td>
                            <td><input type="checkbox" name="upload[<?=$menu->id?>]"
                                <?=in_array('Upload',$permission) ? 'checked' : ''?> value="Upload"></td>
                            <td><input type="checkbox" name="export[<?=$menu->id?>]"
                                <?=in_array('Export',$permission) ? 'checked' : ''?> value="Export"></td>
                          </tr>
                          <?php } } ?>
                        </tbody>
                      </table>
                    </div>
                  </div>
                  <div class="text-center">
                    <button type="submit" class="btn btn-primary">Update</button>
                  </div>
                </div>

              </div>

              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
<script>
$("form#updateForm").submit(function(e) {
  $(':input[type="submit"]').prop('disabled', true);
  e.preventDefault();
  var formData = new FormData(this);
  formData.append("userID", '<?=$user->id?>');

  $.ajax({
    url: $(this).attr('action'),
    type: 'POST',
    data: formData,
    cache: false,
    contentType: false,
    processData: false,
    dataType: 'json',
    success: function(data) {
      if (data.status == 200) {
        //$('.modal').modal('hide');
        toastNotif({
          text: data.message,
          color: '#5bc83f',
          timeout: 5000,
          icon: 'valid'
        });
        $(':input[type="submit"]').prop('disabled', false);
        setTimeout(function() {
          <?php if($this->session->userdata('user_type')=='User'){?>
          location.reload();
          <?php }else{?>
          location.href = "<?=base_url('user/')?>" + data.user_type;
          <?php } ?>
        }, 1000)
      } else if (data.status == 403) {
        toastNotif({
          text: data.message,
          color: '#da4848',
          timeout: 5000,
          icon: 'error'
        });

        $(':input[type="submit"]').prop('disabled', false);
      } else {
        toastNotif({
          text: data.message,
          color: '#da4848',
          timeout: 5000,
          icon: 'error'
        });
        $(':input[type="submit"]').prop('disabled', false);
      }
    },
    error: function() {}
  });
});

function updateStatus(status) {
  alert(status);
}

function getCity(stateID) {
  var userID = <?=$user->id?>;
  $.ajax({
    url: '<?=base_url('Ajax_controller/get_city')?>',
    type: 'POST',
    data: {
      stateID,
      userID
    },
    success: function(data) {
      $('#city').html(data);
    }
  });
}

$(document).ready(function() {
  getCity(<?=$user->state?>)
});
</script>