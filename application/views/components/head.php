<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
    <title><?=$siteinfo->site_name?></title>
    <link rel="shortcut icon" type="image/x-icon" href="assets/img/favicon.png">
    <link rel="stylesheet" href="<?=base_url('public/admin/assets/css/bootstrap.min.css')?>">
    <link rel="stylesheet" href="<?=base_url('public/admin/assets/plugins/bootstrap-tagsinput/css/bootstrap-tagsinput.css')?>">
    <link rel="stylesheet" href="<?=base_url('public/admin/assets/css/font-awesome.min.css')?>">
    <link rel="stylesheet" href="<?=base_url('public/admin/assets/css/feather.css')?>">
    <link rel="stylesheet" href="<?=base_url('public/admin/assets/plugins/select2/css/select2.min.css')?>">
    <link rel="stylesheet" href="<?=base_url('public/admin/assets/css/feathericon.min.css')?>">
    <link rel="stylesheet" href="<?=base_url('public/admin/assets/plugins/morris/morris.css')?>">
    <link rel="stylesheet" href="<?=base_url('public/admin/assets/css/style.css')?>">
    <link rel="stylesheet" href="<?=base_url('public/admin/assets/plugins/datatables/datatables.min.css')?>">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <link rel="stylesheet" href="<?=base_url('public/plugin/toast.css')?>">
    <script src="<?=base_url('public/plugin/toast.js')?>"></script>
    <script src="https://cdn.ckeditor.com/4.16.2/standard/ckeditor.js"></script>
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.3/moment-with-locales.min.js"
integrity="sha512-vFABRuf5oGUaztndx4KoAEUVQnOvAIFs59y4tO0DILGWhQiFnFHiR+ZJfxLDyJlXgeut9Z07Svuvm+1Jv89w5g=="
crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
<!-- include summernote css-->
 <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-lite.min.css" />
 
  <!-- include summernote js-->
    <script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-lite.min.js"></script>
  </head>