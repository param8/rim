<div class="sidebar" id="sidebar">
  <div class="sidebar-inner slimscroll">
    <div id="sidebar-menu" class="sidebar-menu">
      <ul>
        <!-- <li class="menu-title">
                <span><i class="fe fe-home"></i> Main</span>
              </li> -->
        <?php foreach($menus as $menu){
         $menuID = $menu->id;
          $permission = $permissions->$menuID;
          if($permission[0]=='View'){
           if(empty($menu->parent_name)){
          ?>
        <li class="active">
          <a href="<?=base_url($menu->url)?>"><span><?=$menu->icon . $menu->child_name?></span></a>
        </li>
        <?php } 
        if($menu->type == 'dropdown'){
        ?>

        <li class="submenu">
          <a href="#"><span> <?= $menu->child_name == 'Users' ? $menu->icon : ($menu->child_name == 'Project' ? $menu->icon :'' )?>
              <?=$menu->parent_name?></span> <span class="menu-arrow"></span></a>
          <ul style="display: none;">

            <?php 
             if($menu->child_name == 'Users'){
              ?>
              <li><a href="<?=base_url('role')?>">Roles</a></li>
              <?php
            foreach($roles as $role){?>
            <li><a href="<?=base_url('user/'.strtolower($role->name))?>"><?=$role->name?></a></li>
            <?php } if($permission[1]=='Add'){?>
            <li><a href="<?=base_url('create')?>">New</a></li>
            <?php } } if($menu->child_name == 'Project'){?>
              <li><a href="<?=base_url('defect')?>">Defect </a></li>
              <li><a href="<?=base_url('project')?>">Projects</a></li>
              <?php } ?>

          </ul>
        </li>
        <?php
        }
        if($menu->parent_name == 'CONTENT' || $menu->parent_name == 'SETTING'){
          ?>

        <li>
          <a href="<?=base_url($menu->url)?>"><span><?=$menu->icon . $menu->child_name?></span></a>
        </li>

        <?php
        }
       } }?>
      </ul>
    </div>
  </div>
</div>