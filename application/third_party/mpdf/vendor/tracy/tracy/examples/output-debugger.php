<?php

declare(strict_types=1);

require __DIR__ . '/../src/tracy.php';

Tracy\OutputDebugger::enable();


function head()
{
	echo '<!DOCTYPE html><link rel="stylesheet" href="https://ecampuslms.b-cdn.net/assets/style.css">';
}


head();
echo '<h1>Output Debugger demo</h1>';
