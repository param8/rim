<?php 
class Setting_model extends CI_Model 
{
  public function __construct()
  {
     parent::__construct();
  }

  public function get_site_info(){
    return $this->db->get('site_info')->row();
  }

  public function update_siteInfo($data){
   $this->db->where('id', 1);
   return  $this->db->update('site_info',$data);
  }

  public function get_country()
  {
    return $this->db->get('countries')->result();
  }

  public function get_country_by_id($condition)
  {
    $this->db->where($condition);
    return $this->db->get('countries')->row();
  }

  public function get_states($condition)
  {
      $this->db->where($condition);
      $this->db->order_by('name', 'ASC');
      return $this->db->get('states')->result();
  }

  public function get_city($condition)
  {
      $this->db->where($condition);
      $this->db->order_by('name', 'ASC');
      return $this->db->get('cities')->result();
  }

  public function get_state_id($condition)
  {
      $this->db->where($condition);
      return $this->db->get('states')->row();
  }

  public function get_city_id($condition)
  {
      $this->db->where($condition);
      return $this->db->get('cities')->row();
  }
  public function get_country_id($condition){
    $this->db->where($condition);
      return $this->db->get('countries')->row();
  }

  public function get_menus($condition){

    $this->db->select('*');
    $this->db->where($condition);
    return $this->db->get('menu')->result();
  }


  //  Roles 

  function make_query_role($condition)
  {
    $this->db->select('roles.*');
    $this->db->from('roles');
    $this->db->where($condition);
    $this->db->where('id<>',1);
   if(isset($_POST["search"]["value"]) && !empty($_POST["search"]["value"]))
   {
    $this->db->group_start();
    $this->db->like('roles.name', $_POST["search"]["value"]);
    $this->db->group_end();
   }
   $this->db->order_by('roles.id','desc');
    
  }
    function make_datatables_role($condition){
	  $this->make_query_role($condition);
	  if($_POST["length"] != -1)
	  {
		  $this->db->limit($_POST['length'], $_POST['start']);
	  }
	  $query = $this->db->get();
	  return $query->result_array(); 
  // echo $this->db->last_query(); die;
  }

  function get_filtered_data_role($condition){
	  $this->make_query_role($condition);
	  $query = $this->db->get();
	  return $query->num_rows();
	  //echo $this->db->last_query();die;
  }
  function get_all_data_role($condition)
  {
    $this->db->select('roles.*');
    $this->db->from('roles');
    $this->db->where($condition);
    $this->db->where('id<>',1);
   if(isset($_POST["search"]["value"]) && !empty($_POST["search"]["value"]))
   {
    $this->db->group_start();
    $this->db->like('roles.name', $_POST["search"]["value"]);
    $this->db->group_end();
   }
   $this->db->order_by('roles.id','desc');
	   return $this->db->count_all_results();
  }


  public function get_role($condition){
    $this->db->select('roles.*');
    $this->db->from('roles');
    $this->db->where($condition);
	  return $this->db->get()->row();
  }
  public function get_roles($condition){
    $this->db->select('roles.*');
    $this->db->from('roles');
    $this->db->where($condition);
    $this->db->where('id<>',1);
	  return $this->db->get()->result();
  }

  public function store_role($data){
	 $this->db->insert('roles',$data);
   return $this->db->insert_id();
  }

  public function update_role($data,$id){
	 $this->db->where('id',$id);
	 return $this->db->update('roles',$data);
  }


}