<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/userguide3/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'Authantication';
$route['404_override'] = '';
// Web Detail
$route['register'] = 'Authantication/register';
$route['create'] = 'Authantication/create_user';
$route['dashboard'] = 'dashboard';
$route['user/(:any)'] = 'User/index/$1';
$route['role'] = 'Setting/role';
$route['client'] = 'User';
$route['view/(:any)'] = 'User/view/$1';
$route['edit/(:any)'] = 'User/edit/$1';
$route['general-setting'] = 'Setting';
$route['Store-general-form'] = 'Setting/store_siteInfo';
$route['project'] = 'Project';
$route['create-project'] = 'Project/create';
$route['edit-project/(:any)'] = 'Project/edit/$1';
$route['view-project/(:any)'] = 'Project/view/$1';

$route['defect'] = 'Project/observation';
// $route['create-observation'] = 'Project/create_observation';
// $route['edit-observation/(:any)'] = 'Project/edit_observation/$1';
// $route['view-observation/(:any)'] = 'Project/view_observation/$1';
$route['road-inspection-maintenance'] = 'Project/road_inspection_maintenance';
$route['create-road-inspection-maintenance'] = 'Project/create_road_inspection_maintenance';
$route['edit-road-inspection-maintenance/(:any)'] = 'Project/edit_road_inspection_maintenance/$1';
$route['view-road-inspection-maintenance/(:any)'] = 'Project/view_road_inspection_maintenance/$1';


// API Route
$route['signin'] = 'Api/login';
$route['create-road-inspection'] = 'Api/road_inspection';
$route['projects'] = 'Api/projects';
$route['chainages'] = 'Api/chainages';
$route['work-perform'] = 'Api/work_perform';
$route['road-inspections/(:any)'] = 'Api/get_road_inspection/$1';
$route['edit-inspection/(:any)'] = 'Api/edit_road_inspection/$1';