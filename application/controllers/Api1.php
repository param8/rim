<?php
defined('BASEPATH') or exit('No direct script access allowed');
require APPPATH . 'libraries/RestController.php';
require APPPATH . 'libraries/Format.php';
use chriskacerguis\RestServer\RestController;
class Api1 extends RestController
{
  public function __construct()
  {
     parent::__construct();
     header("Access-Control-Allow-Headers: *"); 
     header("Access-Control-Allow-Origin: *");
     $this->load->library("session");
     $this->load->library('pagination');
     $this->load->helper('language');
     $this->load->helper('form');
     $this->load->database();
     $this->load->model("user_model");
     $this->load->model("api_model");
     $this->load->model("project_model");
  }

   public function signin_post()
   {
     $user_name = $this->input->post('user_name');
     $password = $this->input->post('password');
     if(empty($user_name)){
      $status = ['status'=>403,'message'=>'Please enter your username'];
      $this->response($status, RestController::HTTP_BAD_REQUEST);
     }

     if(empty($password)){
      $status = ['status'=>403,'message'=>'Please enter a password'];
      $this->response($status, RestController::HTTP_BAD_REQUEST);
     }

     $user = $this->api_model->login($user_name,$password);
     
     if($user==403){
      $status = ['status'=>403,'message'=>'Inavalid credentials please try again'];
      $this->response($status, RestController::HTTP_BAD_REQUEST);
     }elseif($user==302){
      $status = ['status'=>403,'message'=>'Please contact your administrator'];
      $this->response($status, RestController::HTTP_BAD_REQUEST);
     }else{
      $status = ['status'=>200,'message'=>'Login Successfully']; 
      $this->response($status, RestController::HTTP_OK); 
     }
     
   }

   public function road_inspection_post(){
    $userID = $this->input->post('userID');
    $projectID = $this->input->post('projectID');
		$location = $this->input->post('location');
		$chainage = $this->input->post('chainage');
		$inspection_date = $this->input->post('inspection_date');
		$inspection_time = $this->input->post('inspection_time');
		$work_perform = $this->input->post('work_perform');
		$note = $this->input->post('note');
    $inspector_name = $this->input->post('inspector_name');


    if(empty($projectID)){
      $status = ['status'=>403,'message'=>'Please select project'];
      $this->response($status, RestController::HTTP_BAD_REQUEST);
     }

     if(empty($location)){
      $status = ['status'=>403,'message'=>'Please enter a location'];
      $this->response($status, RestController::HTTP_BAD_REQUEST);
     }

     if(empty($chainage)){
      $status = ['status'=>403,'message'=>'Please select chainage'];
      $this->response($status, RestController::HTTP_BAD_REQUEST);
     }

     if(empty($inspection_date)){
      $status = ['status'=>403,'message'=>'Please enter inspection date'];
      $this->response($status, RestController::HTTP_BAD_REQUEST);
     }

     if(empty($inspection_time)){
      $status = ['status'=>403,'message'=>'Please enter inspection time'];
      $this->response($status, RestController::HTTP_BAD_REQUEST);
     }

     if(empty($work_perform)){
      $status = ['status'=>403,'message'=>'Please  select work performed'];
      $this->response($status, RestController::HTTP_BAD_REQUEST);
     }

     if(empty($note)){
      $status = ['status'=>403,'message'=>'Please  enter note'];
      $this->response($status, RestController::HTTP_BAD_REQUEST);
     }


     $image = $this->input->post('image');

     if(empty($inspector_name)){
      $status = ['status'=>403,'message'=>'Please  enter inspector name'];
      $this->response($status, RestController::HTTP_BAD_REQUEST);
     }

		// $this->load->library('upload');
		// if($_FILES['image']['name'] != '')
		// 	{
		// $config = array(
		// 	'upload_path' 	=> 'uploads/project_inspection',
		// 	'file_name' 	=> uniqid(),
		// 	'allowed_types' => 'jpg|jpeg|png|gif|webp',
		// 	'max_size' 		=> '10000000',
		// );
		// 	$this->upload->initialize($config);
		// if ( ! $this->upload->do_upload('image'))
		// 	{
		// 		$error = $this->upload->display_errors();
		// 		echo json_encode(['status'=>403, 'message'=>$error]);
		// 		exit();
		// 	}
		// 	else
		// 	{
		// 	$type = explode('.',$_FILES['image']['name']);
		// 	$type = $type[count($type) - 1];
		// 	$image = 'uploads/project_inspection/'.$config['file_name'].'.'.$type;
		// 	}
		// }else{
		// 	$status = ['status'=>403,'message'=>'Please  upload image'];
    //   $this->response($status, RestController::HTTP_BAD_REQUEST);
		// }
	

		$data = array(
      'userID'          => $userID,
			'projectID'       => $projectID,
			'location'        => $location,
			'chainage'        => $chainage,
			'inspection_date' => $inspection_date,
			'inspection_time' => $inspection_time,
			'work_perform'     => $work_perform,
			'note'            => $note,
			'image'           => $image,
      'inspector_name'  => $inspector_name,
		);

		$store = $this->project_model->store_road_inspection_maintenance($data);
    
    if($store){
      $status = ['status'=>200,'message'=>'Road inspection created successfully'];
      $this->response($status, RestController::HTTP_OK);
     }else{
      $status = ['status'=>403,'message'=>'Something went wrong']; 
      $this->response($status, RestController::HTTP_BAD_REQUEST); 
     }
     
   }

   public function projects_get(){
    $projects=$this->project_model->get_projects(array('status'=>1));
    if(count($projects)>0){
      $status = ['status'=>200,'data'=>$projects]; 
      $this->response($status, RestController::HTTP_OK); 
    }else{
      $status = ['status'=>403,'message'=>'No Project Found']; 
      $this->response($status, RestController::HTTP_BAD_REQUEST); 
    }
   }

   public function chainages_post(){
    $id = $this->input->post('project_id');
    $chainages=$this->project_model->get_chainages(array('projectID'=>$id,'status'=>1));
    if(count($chainages)>0){
      $status = ['status'=>200,'data'=>$chainages]; 
      $this->response($status, RestController::HTTP_OK); 
    }else{
      $status = ['status'=>403,'message'=>'No chainage Found']; 
      $this->response($status, RestController::HTTP_BAD_REQUEST); 
    }
   }

   public function work_perform_get(){
    $work_performs=$this->project_model->get_observations(array('status'=>1));
    if(count($work_performs)>0){
      $status = ['status'=>200,'data'=>$work_performs]; 
      $this->response($status, RestController::HTTP_OK); 
    }else{
      $status = ['status'=>403,'message'=>'No work performed Found']; 
      $this->response($status, RestController::HTTP_BAD_REQUEST); 
    }
   }


}



        