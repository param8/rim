<?php 
class Ajax_controller extends MY_Controller 
{
	public function __construct()
	{
		parent::__construct();
      $this->load->model('auth_model');
      $this->load->model('user_model');
      $this->load->model('project_model');

	}

	public function get_city()
    {
      $userID = "";
      $userID = $this->input->post('userID');
      $stateID =  $this->input->post('stateID');
     
      if(!empty($userID)){
       $user = $this->user_model->get_user(array('users.id' => $userID));
      }
       $cities = $this->setting_model->get_city(array('state_id'=>$stateID));
       if(count($cities) > 0)
       {
        ?>
        <option value="">Select City</option>
        <?php foreach($cities as $city){ ?>
           <option value="<?=$city->id?>" <?=$user->city == $city->id ? 'selected' : '' ;?>><?=$city->name?></option>
        <?php
        }
       }else
       {
        ?>
        <option value="">No City found</option>
        <?php
       }
    }


    public function get_chainage()
    {
      $inspectionID = "";
      $inspectionID = $this->input->post('inspectionID');
      $projectID =  $this->input->post('projectID');
     
      if(!empty($inspectionID)){
       $inspetion = $this->project_model->get_road_inspection_maintenance(array('road_inspetion_maintenance.id' => $inspectionID));
      }
       $chainages = $this->project_model->get_chainages(array('projectID'=>$projectID));
       if(count($chainages) > 0)
       {
        ?>
        <option value="">Select Chainage</option>
        <?php foreach($chainages as $chainage){ ?>
           <option value="<?=$chainage->chainage?>" <?=$inspetion->chainage == $chainage->chainage ? 'selected' : '' ;?>><?=$chainage->chainage?></option>
        <?php
        }
       }else
       {
        ?>
        <option value="">No chainage found</option>
        <?php
       }
    }
   
    public function set_session_road_inspection(){
      $project = $this->input->post('project');
      $start_date = $this->input->post('start_date');
      $end_date = $this->input->post('end_date');
     
      $this->session->set_userdata('project', $project);
      $this->session->set_userdata('start_date', $start_date);
      $this->session->set_userdata('end_date', $end_date);

      echo json_encode(['status'=>200, 'message'=>'']); 	
			exit();

    }

    public function resetSessionInspection(){
      $this->session->unset_userdata('project');
      $this->session->unset_userdata('start_date');
      $this->session->unset_userdata('end_date');
    }

    public function get_chainage_value(){
      $start_chainage = $this->input->post('start_chainage');
      $end_chainage = $this->input->post('end_chainage');
      if($start_chainage>$end_chainage){
        echo "<p class='text-danger'>End chainage shoud be greater than start chainage</p>";
        exit();
      }
      if(!empty($end_chainage)){
       $values = range($start_chainage, $end_chainage, 0.1);
       for($i=$start_chainage; $i<($end_chainage-.1);  $i=$i+0.1){
        ?>
          <div class="col-6 col-md-6">
            <div class="form-group form-placeholder d-flex">
              <input type="text" class="form-control" name="chainage[]" 
                  placeholder="chainage" value="<?=round($i, 1).'  - '.round($i+.1, 1)?>" readonly>
            </div>
          </div>
        <?php
      }
      }
    }
  }