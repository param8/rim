<?php 
class User extends MY_Controller 
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('user_model');
	}

	public function index()
	{
		$this->not_admin_logged_in();
		$menuID = 2;
		$permission = $this->permissions()->$menuID;
		if($permission[0]=='View'){
		$data['user_type'] = $this->uri->segment(2);
		$data['roles'] = $this->setting_model->get_roles(array('status'=>1));
    $data['page_title'] = $data['user_type'];
		$data['permission'] = $permission;
	  $this->admin_template('users/users',$data);
	}else{
		redirect(base_url('dashboard'));
	}
	}

	public function ajaxUsers(){
		$this->not_admin_logged_in();
		$user_type = $this->uri->segment(3);
		$condition = array('users.status'=>1,'roles.name'=>$user_type);
		$users = $this->user_model->make_datatables($condition); // this will call modal function for fetching data
		$data = array();
		$menuID = 2;
		$permission = $this->permissions()->$menuID;
		foreach($users as $key=>$user) // Loop over the data fetched and store them in array
		{
			$button = '';
			$sub_array = array();
			if($permission[0]=='View'){
			$button .= '<a href="'.base_url('view/'.base64_encode($user['id'])).'" data-bs-toggle="tooltip" data-bs-placement="bottom" title="View '.$user_type.' Detail" class="btn  btn-sm  text-warning"><i class="fa fa-eye"></i></a>';
			}if($permission[2]=='Edit'){
			$button .= '<a href="'.base_url('edit/'.base64_encode($user['id'])).'" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Edit '.$user_type.' Detail" class="btn  btn-sm  text-primary"><i class="fa fa-edit"></i> </a>';
			}if($permission[3]=='Delete'){
			$button .= '<a href="javascript:void(0)" onclick="delete_user('.$user['id'].')" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Delete '.$user_type.'" class="btn  btn-sm  text-danger"><i class="fa fa-trash"></i> </a>';
			}
			$profile_pic = $user['profile_pic'];
			$sub_array[] = $key+1;
			$sub_array[] = $button;
			$sub_array[] = '<img src="'.base_url($profile_pic).'" height="50" width="50">';
			$sub_array[] = $user['name'];
			$sub_array[] = $user['phone'];
			$sub_array[] = $user['email'];
			$sub_array[] = date('d-m-Y', strtotime($user['created_at']));
		
		  $data[] = $sub_array;
		}
	
		$output = array(
			"draw"                    =>     intval($_POST["draw"]),
			"recordsTotal"            =>     $this->user_model->get_all_data($condition),
			"recordsFiltered"         =>     $this->user_model->get_filtered_data($condition),
			"data"                    =>     $data
		);
		
		echo json_encode($output);
	}


	public function edit(){
		$menuID = 2;
		$permission = $this->permissions()->$menuID;
		if($permission[2]=='Edit'){
		$id = base64_decode($this->uri->segment(2));
		$data['user'] = $this->user_model->get_user(array('users.id' => $id));
		$data['states'] = $this->setting_model->get_states(array('country_id'=>101));
    $data['page_title'] = 'Edit '.$data['user']->user_type;
	  $this->admin_template('users/edit',$data);
	}else{
		redirect(base_url('dashboard'));
	}
	}


	public function view(){
		$menuID = 2;
		$permission = $this->permissions()->$menuID;
		if($permission[0]=='View'){
		$id = base64_decode($this->uri->segment(2));
		$data['user'] = $this->user_model->get_user(array('users.id' => $id));
    $data['page_title'] = 'View '.$data['user']->user_type;
	  $this->admin_template('users/view',$data);
	}else{
		redirect(base_url('dashboard'));
	}
	}

	 public function profile(){
		$this->not_logged_in();
		$data['page_title'] = 'Profile';
		$data['panel'] = 'Panel';
		$id = $this->session->userdata('id');
		$data['user'] =$this->user_model->get_user(array('users.id'=>$id));
		$data['states'] = $this->Common_model->get_states(array('country_id'=>101));
		$this->template('web/panel/profile',$data);
	 }

	 public function update(){
		$userID = $this->input->post('userID');
		$name = $this->input->post('name');
		$address = $this->input->post('address');
		$city = $this->input->post('city');
		$state = $this->input->post('state');
		$user = $this->user_model->get_user(array('users.id'=>$userID));
		if(empty($name)){
			echo json_encode(['status'=>403, 'message'=>'Please enter your name']); 	
			exit();
		}

	
		$this->load->library('upload');
		if($_FILES['profile_pic']['name'] != '')
			{
		$config = array(
			'upload_path' 	=> 'uploads/user',
			'file_name' 	=> str_replace(' ','',$name).uniqid(),
			'allowed_types' => 'jpg|jpeg|png|gif|webp',
			'max_size' 		=> '10000000',
		);
			$this->upload->initialize($config);
		if ( ! $this->upload->do_upload('profile_pic'))
			{
				$error = $this->upload->display_errors();
				echo json_encode(['status'=>403, 'message'=>$error]);
				exit();
			}
			else
			{
			$type = explode('.',$_FILES['profile_pic']['name']);
			$type = $type[count($type) - 1];
			$profile_pic = 'uploads/user/'.$config['file_name'].'.'.$type;
			}
		}else{
			if(!empty($user->profile_pic)){
				$profile_pic = $user->profile_pic;
			}else{
				$profile_pic = 'public/dummy_user.png';
			}
			
		}

		$menus = $this->menus();
    $permission = array();
		foreach($menus as $menu){
			if($menu->child_name=='Dashboard'){
				$permission[$menu->id] = array('View','Add','Edit','Delete','Upload','Export');
			}else{
			$view = !empty($this->input->post('view')[$menu->id]) ? $this->input->post('view')[$menu->id] : '';
			$add = !empty($this->input->post('add')[$menu->id]) ? $this->input->post('add')[$menu->id] : '';
			$edit = !empty($this->input->post('edit')[$menu->id]) ? $this->input->post('edit')[$menu->id] : '';
			$delete = !empty($this->input->post('delete')[$menu->id]) ? $this->input->post('delete')[$menu->id] : '';
			$upload = !empty($this->input->post('upload')[$menu->id]) ? $this->input->post('upload')[$menu->id] : '';
			$export = !empty($this->input->post('export')[$menu->id]) ? $this->input->post('export')[$menu->id] : '';
			$permission[$menu->id] = array($view,$add,$edit,$delete,$upload,$export);
			}
		}
		$permission_json = json_encode($permission);
		
		$data = array(
			'name'       => $name,
			'address'    => $address ,
			'city'       => $city,
			'state'      => $state,
			'profile_pic'=> $profile_pic,
			'permission'   => $permission_json
		);
		$update = $this->user_model->update_user($data,$userID);
		if($update){
			echo json_encode(['status'=>200, 'message'=>$user->user_type.'  updated successfully!','user_type'=>strtolower($user->roleName)]);
		}else{
			echo json_encode(['status'=>302, 'message'=>'something wrong happened']);   
		}
	
		}

		public function reset_password(){
			$this->not_logged_in();
			$data['page_title'] = 'Profile';
			$data['panel'] = 'Panel';
			$this->template('web/panel/reset-password',$data);
		}

		public function update_password(){
			$userID = $this->session->userdata('id');
			$old_password = $this->input->post('old_password');
			$new_password = $this->input->post('new_password');
			$confirm_password = $this->input->post('confirm_password');
      
			$user = $this->user_model->get_user(array('users.id' =>$userID));

			if(empty($old_password)){
				echo json_encode(['status'=>403, 'message'=>'Please enter old password']);  	
				exit();
			}

			if($user->password != md5($old_password)){
				echo json_encode(['status'=>403, 'message'=>'Old password is incorrect']);  	
				exit();
			}

			if(empty($new_password)){
				echo json_encode(['status'=>403, 'message'=>'Please enter new password']);  	
				exit();
			}

			if(strlen($new_password)<6){
				echo json_encode(['status'=>403, 'message'=>'Password minimum length must be at least 6 characters']);  	
				exit();
			}

			if(empty($confirm_password)){
				echo json_encode(['status'=>403, 'message'=>'Please enter confirm password']);  	
				exit();
			}


			if($new_password != $confirm_password){
				echo json_encode(['status'=>403, 'message'=>'New Password and Confirm Password must be the same']);  	
				exit();
			}

			$data = array(
				'password' => md5($new_password)
			);
   
			$update = $this->user_model->update_user($data,$userID);
			if($update){
		  	echo json_encode(['status'=>200, 'message'=>'Password updated successfully please login again !..']); 
			}else{
				echo json_encode(['status'=>302, 'message'=>'something wrong happened']);   
			}
		}

		public function forget_password()
		{
			$data['page_title'] = 'Forget Password';	
		  $this->template('web/forget-password',$data);
		}

		public function verify_detail(){
			$user_detail = $this->input->post('user_detail');
     
			if(empty($user_detail)){
				echo json_encode(['status'=>403, 'message'=>'Please enter user detail']);  	
				exit();
			}

			$users = $this->user_model->get_user_detail($user_detail);

			if($users->num_rows() <=0){
				echo json_encode(['status'=>403, 'message'=>'Your details are not valid please try again']);   	
				exit();
			}

			$user = $users->row();
      $otp = rand(0,999999);
			$subject = "Welcome to Shikshakul";
		  $html = 'Dear '.$user->name.',<br>
			Your OTP for reset your password to Shikshakul Classes is '.$otp.' <br><br>
			Best Regards,<br>
			Shikshakul Team<br>';
		 $sendEmail =	sendEmail($user->email,$subject,$html);
      
		 $templateID = '1707168795609801101';
		 $message = urlencode('Your OTP for reset your password to Shikshakul Classes is '.$otp);
		 $sendSMS =	 sendSMS($user->phone,$message,$templateID);
     $decode_sms =  json_decode($sendSMS);
		if($sendEmail==1 && $decode_sms->statusCode==200){
			$session_data = array(
       'detail'     => $user_detail,
			 'verify_otp' => $otp
			);
			$this->session->set_userdata($session_data);
			echo json_encode(['status'=>200, 'message'=>'Password updated successfully please login again !..']); 
		}else{
			echo json_encode(['status'=>403, 'message'=>'Please try again OTP not send !..']); 
		}

		}

		public function change_password(){
			$data['page_title'] = 'Change Password';	
		  $this->template('web/change-password',$data);
		}

		public function password_change(){
			$verify_otp = $this->session->userdata('verify_otp');
			$detail = $this->session->userdata('detail');
			$otp = $this->input->post('otp');
			$new_password = $this->input->post('new_password');
			$confirm_password = $this->input->post('confirm_password');
      
			$users = $this->user_model->get_user_detail($detail);

			$userID = $users->row()->id;

			if(empty($otp)){
				echo json_encode(['status'=>403, 'message'=>'Please enter  OTP']);  	
				exit();
			}

			if($verify_otp != $otp){
				echo json_encode(['status'=>403, 'message'=>'Please enter valid OTP']);  	
				exit();
			}

			if(empty($new_password)){
				echo json_encode(['status'=>403, 'message'=>'Please enter new password']);  	
				exit();
			}

			if(strlen($new_password)<6){
				echo json_encode(['status'=>403, 'message'=>'Password minimum length must be at least 6 characters']);  	
				exit();
			}

			if(empty($confirm_password)){
				echo json_encode(['status'=>403, 'message'=>'Please enter confirm password']);  	
				exit();
			}

			if($new_password != $confirm_password){
				echo json_encode(['status'=>403, 'message'=>'New Password and Confirm Password must be the same']);  	
				exit();
			}

			$data = array(
				'password' => md5($new_password)
			);
   
			$update = $this->user_model->update_user($data,$userID);
			if($update){
		  	echo json_encode(['status'=>200, 'message'=>'Password updated successfully please login again !..']); 
			}else{
				echo json_encode(['status'=>302, 'message'=>'something wrong happened']);   
			}
		}
	
}