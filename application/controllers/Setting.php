<?php
class Setting extends MY_Controller 
{
    public function __construct()
    {
      parent::__construct();
      $this->load->model('setting_model');
      $this->load->library('csvimport');
    }

    public function index(){
     $menuID = 7;
		 $permission = $this->permissions()->$menuID;
		 if($permission[0]=='View'){
      $data['page_title'] = 'General-Setting';
      $this->admin_template('components/breadcrumb',$data);
      $this->admin_template('setting/setting',$data);
     }else{
			redirect(base_url('dashboard'));
		 }
    }


    public function store_siteInfo(){
      $menuID = 7;
      $permission = $this->permissions()->$menuID;
      if($permission[2]!='Edit'){
        echo json_encode(['status'=>403, 'message'=>'You have no permissions to edit this']); 	
        exit();
      }
       $site_name = $this->input->post('site_name');
       $site_contact = $this->input->post('mobile');
       $whatsapp_no = $this->input->post('whatsapp');
       $site_email = $this->input->post('email');
       $site_address = $this->input->post('address');
       $footer_contant = $this->input->post('footer_content');
       $discription = $this->input->post('description');
       $siteinfo = $this->setting_model->get_site_info();
       if(empty($site_name)){
        echo json_encode(['status'=>403, 'message'=>'Please enter site name']); 	
        exit();
       }
       if(empty($site_contact)){
        echo json_encode(['status'=>403, 'message'=>'Please enter site mobile']); 	
        exit();
       }
       if(empty($site_email)){
        echo json_encode(['status'=>403, 'message'=>'Please enter site email']); 	
        exit();
       }

       if(strlen((string)$site_contact) != 10){
        echo json_encode(['status'=>403, 'message'=>'Please enter mobile number 10 digits']);
        exit();
       }
       if(!empty($whatsapp_no)){
       if(strlen((string)$whatsapp_no) != 10){
        echo json_encode(['status'=>403, 'message'=>'Please enter whatsapp number 10 digits']);
        exit();
       }
      }
      if(empty($footer_contant)){
        echo json_encode(['status'=>403, 'message'=>'Please enter footer content']); 	
        exit();
       }

       if(empty($discription)){
        echo json_encode(['status'=>403, 'message'=>'Please enter description']); 	
        exit();
       }
       
       $this->load->library('upload');
      if(!empty($_FILES['image']['name'])){
      $config = array(
        'upload_path' 	=> 'uploads/siteInfo',
        'file_name' 	=> str_replace(' ','',$site_name).uniqid(),
        'allowed_types' => 'jpg|jpeg|png|gif',
        'max_size' 		=> '10000000',
      );
      $this->upload->initialize($config);
      if ( ! $this->upload->do_upload('image'))
        {
            $error = $this->upload->display_errors();
            echo json_encode(['status'=>403, 'message'=>$error]); 	
            exit();
        }
        else
        {
          $type = explode('.', $_FILES['image']['name']);
          $type = $type[count($type) - 1];
          $image = 'uploads/siteInfo/'.$config['file_name'].'.'.$type;
        }
      }elseif(!empty($siteinfo->site_logo)){
        $image = $siteinfo->site_logo;
      }else{
        $image = 'public/website/images/dummy_image.jpg';
      }
      $data = array(
       'site_name' => $site_name,
       'site_email' => $site_email,
       'site_contact' => $site_contact,
       'whatsapp_no' => $whatsapp_no,
       'site_address' => $site_address,
       'discription' => $discription,
       'footer_contant' => $footer_contant,
       'site_logo' => $image,
      );

      $update = $this->setting_model->update_siteInfo($data);

      if($update){
        echo json_encode(['status'=>200, 'message'=>'Site-info update successfully!']);
    }else{
        echo json_encode(['status'=>302, 'message'=>'Something wrong happened']);   
    }

    }

    public function role(){
    $menuID = 3;
		$permission = $this->permissions()->$menuID;
		if($permission[0]=='View'){
      $data['page_title'] = 'Role';
      $data['permission'] = $permission;
      $this->admin_template('role/index',$data);
    }else{
			redirect(base_url('dashboard'));
		}
    }

    public function ajaxRole(){
      $condition = array('status'=>1);
      $roles = $this->setting_model->make_datatables_role($condition); // this will call modal function for fetching data
      $data = array();
      $menuID = 3;
		  $permission = $this->permissions()->$menuID;
		
      foreach($roles as $key=>$role) // Loop over the data fetched and store them in array
      {
        $button = '';
        $sub_array = array();
        if($permission[0]=='View'){
          $button .= '<a href="javascript:void(0)" onclick="view_role('.$role['id'].')" data-bs-toggle="tooltip" data-bs-placement="bottom" title="View observation Detail" class="btn  btn-sm  text-warning"><i class="fa fa-eye"></i></a>';
        }if($permission[2]=='Edit'){
         $button .= '<a href="javascript:void(0)" onclick="edit_role('.$role['id'].')" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Edit observation Detail" class="btn  btn-sm  text-primary"><i class="fa fa-edit"></i> </a>';
        } 
         $sub_array[] = $key+1;
        $sub_array[] = $role['name'];
        $sub_array[] = date('d-m-Y', strtotime($role['created_at']));
        $sub_array[] = $button;
        $data[] = $sub_array;
      }
    
      $output = array(
        "draw"                    =>     intval($_POST["draw"]),
        "recordsTotal"            =>     $this->setting_model->get_all_data_role($condition),
        "recordsFiltered"         =>     $this->setting_model->get_filtered_data_role($condition),
        "data"                    =>     $data
      );
      
      echo json_encode($output);
    }
  
  
    public function store_role(){
      $name = $this->input->post('name');
      if(empty($name)){
        echo json_encode(['status'=>403, 'message'=>'Please enter role name']); 	
        exit();
      }
      $role = $this->setting_model->get_role(array('name'=>$name));
      if($role){
        echo json_encode(['status'=>403, 'message'=>'This role already exists']); 	
        exit();
      }
  
      $data = array(
        'name'=>$name,
      );
  
      $store = $this->setting_model->store_role($data);
  
      if($store){
        echo json_encode(['status'=>200, 'message'=>'Role created successfully']); 	
        exit();
      }else{
        echo json_encode(['status'=>403, 'message'=>'Something went wrong']); 	
        exit();
      }
    }
  
    public function role_edit_form(){
      $id = $this->input->post('id');
      $role = $this->setting_model->get_role(array('id'=>$id));
      ?>
      <div class="form-group">
        <label for="recipient-name" class="col-form-label">Role Name:</label>
        <input type="text" class="form-control" name="name" id="name" value="<?=$role->name?>"
          placehoder="Role Name">
      </div>
  
      <input type="hidden" name="id" id="id" value="<?=$id?>">
     <?php
    }
    
  
    public function update_role(){
      $id = $this->input->post('id');
      $name = $this->input->post('name');
      if(empty($name)){
        echo json_encode(['status'=>403, 'message'=>'Please enter role name']); 	
        exit();
      }
      $role = $this->setting_model->get_role(array('name'=>$name,'id<>'=>$id));
      if($observation){
        echo json_encode(['status'=>403, 'message'=>'Role already exists']); 	
        exit();
      }
  
      $data = array(
        'name'=>$name,
      );
  
      $update = $this->setting_model->update_role($data,$id);
  
      if($update){
        echo json_encode(['status'=>200, 'message'=>'Role updated successfully']); 	
        exit();
      }else{
        echo json_encode(['status'=>403, 'message'=>'Something went wrong']); 	
        exit();
      }
    }
  
  
    public function role_view_form(){
      $id = $this->input->post('id');
      $role = $this->setting_model->get_role(array('id'=>$id));
      ?>
      <div class="form-group">
        <label for="recipient-name" class="col-form-label">Role Name:</label>
        <input type="text" class="form-control" name="name" id="name" readonly value="<?=$role->name?>" >
      </div>
     <?php
    }

}
?>