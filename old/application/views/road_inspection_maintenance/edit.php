<div class="main-wrapper">
  <div class="page-wrapper">
    <div class="content container-fluid">
      <div class="page-header">
        <div class="row">
          <div class="col-md-12">
            <div class="card">
              <div class="card-header">
                <h4 class="card-title"><?=$page_title?></h4>
              </div>
              <div class="card-body pt-0">
                <form id="inspectionform" method="post" action="<?=base_url('Project/update_road_inspection_maintenance');?>"
                  enctype="multipart/form-data">
                  <div class="settings-form row">
                    <div class="col-md-6">
                      <div class="form-group">
                        <label>Project Name<span class="star-red">*</span></label>
                        <select class="form-control" name="projectID" id="projectID" onchange="getChainage(this.value)">
                          <option value="">Select Project</option>
                          <?php foreach($projects as $project){?>
                          <option value="<?=$project->id?>" <?=$inspection->projectID==$project->id ? 'selected' : '';?>><?=$project->name?></option>
                          <?php } ?>
                        </select>
                      </div>
                    </div>

                    <div class="col-md-6">
                      <div class="form-group">
                        <label>Location<span class="star-red">*</span></label>
                        <input type="text" class="form-control" name="location" id="location" value="<?=$inspection->location?>" placeholder="Location">
                      </div>
                    </div>

                    <div class="col-md-6">
                      <div class="form-group">
                        <label>Chainage<span class="star-red">*</span></label>
                        <select class="form-control" name="chainage" id="chainage">
                          <option value="">Select Chainage</option>
                        </select>
                      </div>
                    </div>

                    <div class="col-md-6">
                      <div class="form-group">
                        <label>Inspection Date<span class="star-red">*</span></label>
                        <input type="text" onfocus="(this.type='date')" class="form-control" name="inspection_date"
                          id="inspection_date" value="<?=$inspection->inspection_date?>" placeholder="Inspection Date">
                      </div>
                    </div>

                    <div class="col-md-6">
                      <div class="form-group">
                        <label>Inspection Time<span class="star-red">*</span></label>
                        <input type="text" onfocus="(this.type='time')" class="form-control" name="inspection_time"
                          id="inspection_time" value="<?=$inspection->inspection_time?>" placeholder="Inspection Time">
                      </div>
                    </div>

                    <div class="col-md-6">
                      <div class="form-group">
                        <label>Work Performed<span class="star-red">*</span></label>
                        <select class="form-control" name="work_perform" id="work_perform">
                          <option value="">Select Work Performed</option>
                          <?php foreach($observations as $observation){?>
                          <option value="<?=$observation->id?>" <?=$inspection->work_perform==$observation->id ? 'selected' : '';?>><?=$observation->name?></option>
                          <?php } ?>
                        </select>
                      </div>
                    </div>

                    <div class="col-md-6">
                      <div class="form-group">
                        <label>Note<span class="star-red">*</span></label>
                        <textarea class="form-control" name="note" id="note" placeholder="Note"> <?=$inspection->note?></textarea>
                      </div>
                    </div>

                    <div class="col-md-6">
                      <div class="form-group">
                        <label>Image<span class="star-red">*</span></label>
                        <input type="file" class="form-control" name="image" id="image" accept="image/*" placeholder="Image">
                        <div>
                          <img src="<?=base_url($inspection->image)?>" width="100" height="100">
                        </div>
                      </div>
                    </div>

                    <div class="col-md-6">
                      <div class="form-group">
                        <label> Inspector Name<span class="star-red">*</span></label>
                        <input type="text"  class="form-control" name="inspector_name"
                          id="inspector_name" value="<?=$inspection->inspector_name?>" placeholder="Inspector Name">
                      </div>
                    </div>

                    <div class="form-group mb-0">
                      <div class="settings-btns">
                        <button type="submit" class="btn btn-orange">Submit</button>
                        <!-- <button type="reset" class="btn btn-grey">Cancel</button> -->
                      </div>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<script>
$("form#inspectionform").submit(function(e) {
  $(':input[type="submit"]').prop('disabled', true);
  e.preventDefault();
  var formData = new FormData(this);
  formData.append("id", '<?=$inspection->id?>');
  $.ajax({
    url: $(this).attr('action'),
    type: 'POST',
    data: formData,
    cache: false,
    contentType: false,
    processData: false,
    dataType: 'json',
    success: function(data) {
      if (data.status == 200) {
        //$('.modal').modal('hide');
        toastNotif({
          text: data.message,
          color: '#5bc83f',
          timeout: 5000,
          icon: 'valid'
        });
        $(':input[type="submit"]').prop('disabled', false);
        setTimeout(function() {

          location.href = "<?=base_url('road-inspection-maintenance')?>";

        }, 1000)
      } else if (data.status == 403) {
        toastNotif({
          text: data.message,
          color: '#da4848',
          timeout: 5000,
          icon: 'error'
        });

        $(':input[type="submit"]').prop('disabled', false);
      } else {
        toastNotif({
          text: data.message,
          color: '#da4848',
          timeout: 5000,
          icon: 'error'
        });
        $(':input[type="submit"]').prop('disabled', false);
      }
    },
    error: function() {}
  });
});

function getChainage(projectID) {
  var inspectionID = '<?=$inspection->id?>';
  $.ajax({
    url: '<?=base_url('Ajax_controller/get_chainage')?>',
    type: 'POST',
    data: {
      projectID,
      inspectionID
    },
    success: function(data) {
      $('#chainage').html(data);
    }
  });
}

$(document).ready(function() {
  getChainage(<?=$inspection->projectID?>);
});
</script>