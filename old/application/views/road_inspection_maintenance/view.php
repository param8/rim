<div class="main-wrapper">
  <div class="page-wrapper">
    <div class="content container-fluid">
      <div class="page-header">
        <div class="row">
          <div class="col-md-12">
            <div class="card">
              <div class="card-header">
                <h4 class="card-title"><?=$page_title?></h4>
              </div>
              <div class="card-body pt-0">
                <form id="" method="post" 
                  enctype="multipart/form-data">
                  <div class="settings-form row">
                    <div class="col-md-6">
                      <div class="form-group">
                        <label>Project Name<span class="star-red">*</span></label>
                        <select class="form-control" name="projectID" id="projectID" readonly onchange="getChainage(this.value)" disabled>
                          <option value="">Select Project</option>
                          <?php foreach($projects as $project){?>
                          <option value="<?=$project->id?>" <?=$inspection->projectID==$project->id ? 'selected' : '';?>><?=$project->name?></option>
                          <?php } ?>
                        </select>
                      </div>
                    </div>

                    <div class="col-md-6">
                      <div class="form-group">
                        <label>Location<span class="star-red">*</span></label>
                        <input type="text" class="form-control" name="location" id="location" readonly value="<?=$inspection->location?>" placeholder="Location">
                      </div>
                    </div>

                    <div class="col-md-6">
                      <div class="form-group">
                        <label>Chainage<span class="star-red">*</span></label>
                        <select class="form-control" name="chainage" id="chainage" disabled>
                          <option value="">Select Chainage</option>
                        </select>
                      </div>
                    </div>

                    <div class="col-md-6">
                      <div class="form-group">
                        <label>Inspection Date<span class="star-red">*</span></label>
                        <input type="text" onfocus="(this.type='date')" class="form-control" name="inspection_date"
                          id="inspection_date" value="<?=$inspection->inspection_date?>" readonly placeholder="Inspection Date">
                      </div>
                    </div>

                    <div class="col-md-6">
                      <div class="form-group">
                        <label>Inspection Time<span class="star-red">*</span></label>
                        <input type="text" onfocus="(this.type='time')" class="form-control" name="inspection_time"
                          id="inspection_time" value="<?=$inspection->inspection_time?>" readonly placeholder="Inspection Time">
                      </div>
                    </div>

                    <div class="col-md-6">
                      <div class="form-group">
                        <label>Work Performed<span class="star-red">*</span></label>
                        <select class="form-control" name="observation" id="observation" disabled>
                          <option value="">Select Work Performed</option>
                          <?php foreach($observations as $observation){?>
                          <option value="<?=$observation->id?>" <?=$inspection->work_perform==$observation->id ? 'selected' : '';?>><?=$observation->name?></option>
                          <?php } ?>
                        </select>
                      </div>
                    </div>

                    <div class="col-md-6">
                      <div class="form-group">
                        <label>Note<span class="star-red">*</span></label>
                        <textarea class="form-control" name="note" id="note" placeholder="Note" readonly> <?=$inspection->note?></textarea>
                      </div>
                    </div>

                    <div class="col-md-6">
                      <div class="form-group">
                        <label>Image<span class="star-red">*</span></label>
                        <div>
                          <img src="<?=base_url($inspection->image)?>" width="100" height="100">
                        </div>
                      </div>
                    </div>

                    <div class="col-md-6">
                      <div class="form-group">
                        <label> Inspector Name<span class="star-red">*</span></label>
                        <input type="text"  class="form-control" name="inspector_name"
                          id="inspector_name" value="<?=$inspection->inspector_name?>" readonly placeholder="Inspector Name">
                      </div>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<script>


function getChainage(projectID) {
  var inspectionID = '<?=$inspection->id?>';
  $.ajax({
    url: '<?=base_url('Ajax_controller/get_chainage')?>',
    type: 'POST',
    data: {
      projectID,
      inspectionID
    },
    success: function(data) {
      $('#chainage').html(data);
    }
  });
}

$(document).ready(function() {
  getChainage(<?=$inspection->projectID?>);
});
</script>