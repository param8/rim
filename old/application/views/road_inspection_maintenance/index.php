<div class="main-wrapper">
  <div class="page-wrapper">
    <div class="content container-fluid">
      <div class="page-header">
        <div class="row">
          <div class="col-sm-6">
            <h3 class="page-title"><?=$page_title?></h3>
            <ul class="breadcrumb">
              <li><a href="<?=base_url('dashboard')?>">Dashboard/</a></li>
              <li class="breadcrumb-item"><a href="javascript:(0);"><?=$page_title?></a></li>
            </ul>
          </div>
          <?php if($permission[1]=='Add'){?>
          <div class="col-sm-3">
            <div class="float-right">
              <a class="btn btn-primary btn-sm" href="<?=base_url('create-road-inspection-maintenance')?>"
                style="float: right">Create <?=$page_title?></a>
            </div>
          </div>
          <?php } if($permission[5]=='Export'){?>
          <div class="col-sm-3">
            <div class="float-right">
              <button class="btn btn-warning btn-sm" type="button" onclick="downloadExel()" style="float:right">Download
                Exel</button>
            </div>
          </div>
          <?php } ?>
        </div>
      </div>
      <div class="row">
        <form id="setSessionInspectionform" method="post" action="<?=base_url('Ajax_controller/set_session_road_inspection');?>"
          enctype="multipart/form-data">
          <div class="settings-form row">
            <div class="col-md-3">
              <div class="form-group">
                <label>Projects</label>
                <select class="form-select" name="project" id="project" >
                  <option value="">Select Project</option>
                  <?php foreach($projects as $project){?>
                    <option value="<?=$project->id?>" <?=$this->session->userdata('project') == $project->id ? 'selected' : ''?>><?=$project->name?></option>
                    <?php } ?>
                </select>
                <!-- <input type="text" class="form-control" name="location" id="location"
                  value="<?=$this->session->userdata('location')?>" placeholder="Location"> -->
              </div>
            </div>

            <div class="col-md-3">
              <div class="form-group">
                <label>Start Date</label>
                <input type="text" onfocus="(this.type='date')" class="form-control" name="start_date" id="start_date"
                  value="<?=$this->session->userdata('start_date')?>" placeholder="Start Date">
              </div>
            </div>

            <div class="col-md-3">
              <div class="form-group">
                <label>End Date</label>
                <input type="text" onfocus="(this.type='date')" class="form-control" name="end_date" id="end_date"
                  value="<?=$this->session->userdata('end_date')?>" placeholder="End Date">
              </div>
            </div>

            <div class="col-md-3">
              <div class="form-group">
              <label class="hidden1">asfs </label>
                <div class="form-group mb-0">
                  <div class="settings-btns">
                    <button type="submit" class="btn btn-orange  ">Search</button>
                    <a href="javascript:void(0)" onclick="resetSessionInspection()" class="btn btn-grey  ">Reset</a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </form>
      </div>
      <div class="row">
        <div class="col-sm-12">
          <div class="card">
            <div class="card-body">
              <div class="table-responsive">
                <table class=" table table-hover table-center mb-0" id="inspectionDataTable">
                  <thead>
                    <tr>
                      <th>Sr.no.</th>
                      <th>Chainage</th>
                      <th>Latitude</th>
                      <th>Longitude</th>
                      <th>Date</th>
                      <th>Time</th>
                      <th>Defect </th>
                      <th>Notes</th>
                      <th>Inspected By</th>
                      <th>Image</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tbody>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<script>
// $(document).ready(function() {
//   var dataTable = $('#inspectionDataTable').DataTable({
//     "processing": true,
//     "serverSide": true,
//     buttons: [{
//       extend: 'excelHtml5',
//       text: 'Download Excel'
//     }],
//     "order": [[3, 'desc']],
//     "ajax": {
//       url: "<?//=base_url('Project/ajaxRoadInspectionMaintenance')?>",
//       type: "POST"
//     },
//     "columnDefs": [{
//       "targets": [0],
//       "orderable": true,
//     }, ],
//   });
// });

$(document).ready(function() {
  var dataTable = $('#inspectionDataTable').DataTable({
    order: [],
    // "processing": true,
    // "serverSide": true,
    // buttons: [{
    //     extend: 'excelHtml5',
    //     text: 'Download Excel'
    // }],
    
    "ajax": {
      url: "<?=base_url('Project/ajaxRoadInspectionMaintenance')?>",
      type: "POST",
    },
    // "columnDefs": [{
    //           "targets": [0],
    //           "orderable": true,
    //       }, ],
  });
});


function delete_road_inspection_maintenance(id) {

  Swal.fire({
    title: 'Are you sure delete project?',
    text: "You won't be able to revert this!",
    type: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Yes, delete it!'
  }).then((result) => {
    if (result.value) {
      $.ajax({
        url: '<?=base_url('Project/delete_road_inspection_maintenance')?>',
        type: 'POST',
        data: {
          id
        },
        dataType: 'json',
        success: function(data) {
          if (data.status == 200) {
            toastNotif({
              text: data.message,
              color: '#5bc83f',
              timeout: 5000,
              icon: 'valid'
            });
            setTimeout(function() {

              location.reload();

            }, 1000)


          } else if (data.status == 302) {
            toastNotif({
              text: data.message,
              color: '#da4848',
              timeout: 5000,
              icon: 'error'
            });

          }
        }
      })
    }
  })
}

function downloadExel() {
  var messageText = "You want download  Project detail!";
  var confirmText = 'Yes, download it!';
  var message = "Project detail download Successfully!";

  Swal.fire({
    title: 'Are you sure?',
    text: messageText,
    icon: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: confirmText
  }).then((result) => {
    if (result.isConfirmed) {

      location.href = "<?=base_url('Project/export_project')?>";
    }
  })

}


$("form#setSessionInspectionform").submit(function(e) {
  $(':input[type="submit"]').prop('disabled', true);
  e.preventDefault();
  var formData = new FormData(this);
  $.ajax({
    url: $(this).attr('action'),
    type: 'POST',
    data: formData,
    cache: false,
    contentType: false,
    processData: false,
    dataType: 'json',
    success: function(data) {
      if (data.status == 200) {
        //$('.modal').modal('hide');
        toastNotif({
          text: data.message,
          color: '#5bc83f',
          timeout: 5000,
          icon: 'valid'
        });
        $(':input[type="submit"]').prop('disabled', false);
        setTimeout(function() {

          location.reload();

        }, 1000)
      } else if (data.status == 403) {
        toastNotif({
          text: data.message,
          color: '#da4848',
          timeout: 5000,
          icon: 'error'
        });

        $(':input[type="submit"]').prop('disabled', false);
      } else {
        toastNotif({
          text: data.message,
          color: '#da4848',
          timeout: 5000,
          icon: 'error'
        });
        $(':input[type="submit"]').prop('disabled', false);
      }
    },
    error: function() {}
  });
});

function resetSessionInspection(){
  $.ajax({
       url: '<?=base_url("Ajax_controller/resetSessionInspection")?>',
       type: 'POST',
       data: {ResetSesession:'ResetSesession'},  
       success: function (data) {
        location.reload();
       },
     });
}

</script>