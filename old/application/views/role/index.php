<div class="main-wrapper">
  <div class="page-wrapper">
    <div class="content container-fluid">
      <div class="page-header">
        <div class="row">
          <div class="col-sm-6">
            <h3 class="page-title"><?=$page_title?></h3>
            <ul class="breadcrumb">
              <li><a href="<?=base_url('dashboard')?>">Dashboard/</a></li>
              <li class="breadcrumb-item"><a href="javascript:(0);"><?=$page_title?></a></li>
            </ul>
          </div>
          <?php if($permission[1]=='Add'){?>
          <div class="col-sm-6">
            <div class="float-right">
             <a  class="btn btn-primary btn-sm" href="javascript:void(0)" onclick="roleModal()" style="float: right">Create <?=$page_title?></a>
           </div>
          </div>
          <?php } ?>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-12">
          <div class="card">
            <div class="card-body">
              <div class="table-responsive">
                <table class=" table table-hover table-center mb-0" id="roleDataTable">
                  <thead>
                    <tr>
                      <th>S.no.</th>
                      <th>Name</th>
                      <th>Created_at</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tbody>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="roleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
  aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Create <?=$page_title?></h5>
        <button type="button" class="close" data-dismiss="modal" onclick="closeCreateModal()" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="createRoleForm" action="<?=base_url('Setting/store_role')?>" method="POST"
          enctype="multipart/form-data">
          <div class="form-group">
            <label for="recipient-name" class="col-form-label">Role Name:</label>
            <input type="text" class="form-control" name="name" id="name" placehoder="Role Name">
          </div>

      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-primary">Submit</button>
      </div>
      </form>
    </div>
  </div>
</div>

<!-- Edit Observation Modal Start -->

<div class="modal fade" id="editRoleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
  aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Edit <?=$page_title?></h5>
        <button type="button" class="close" data-dismiss="modal" onclick="closeEditModal()" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form id="editRoleForm" action="<?=base_url('Setting/update_role')?>" method="POST" enctype="multipart/form-data">
      <div class="modal-body" id="show_edit_data">
         

      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-primary">Submit</button>
      </div>
      </form>
    </div>
  </div>
</div>

<!-- Edit Observation Model End -->

<!-- View Observation Modal Start -->

<div class="modal fade" id="viewRoleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
  aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">View Observation</h5>
        <button type="button" class="close" data-dismiss="modal" onclick="closeViewModal()" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" id="show_view_data"> 
      </div>
    </div>
  </div>
</div>

<!-- View Quiz Type Model End -->


<script>
$(document).ready(function() {
  var dataTable = $('#roleDataTable').DataTable({
    "processing": true,
    "serverSide": true,
    buttons: [{
      extend: 'excelHtml5',
      text: 'Download Excel'
    }],
    "order": [],
    "ajax": {
      url: "<?=base_url('Setting/ajaxRole')?>",
      type: "POST"
    },
    "columnDefs": [{
      "targets": [0],
      "orderable": false,
    }, ],
  });
});

function roleModal(){
 $('#roleModal').modal('show');
}

$("form#createRoleForm").submit(function(e) {
  $(':input[type="submit"]').prop('disabled', true);
  e.preventDefault();
  var formData = new FormData(this);
  $.ajax({
    url: $(this).attr('action'),
    type: 'POST',
    data: formData,
    cache: false,
    contentType: false,
    processData: false,
    dataType: 'json',
    success: function(data) {
      if (data.status == 200) {
        //$('.modal').modal('hide');
        toastNotif({
          text: data.message,
          color: '#5bc83f',
          timeout: 5000,
          icon: 'valid'
        });
        $(':input[type="submit"]').prop('disabled', false);
        setTimeout(function() {

          location.href = "<?=base_url('role')?>";

        }, 1000)
      } else if (data.status == 403) {
        toastNotif({
          text: data.message,
          color: '#da4848',
          timeout: 5000,
          icon: 'error'
        });

        $(':input[type="submit"]').prop('disabled', false);
      } else {
        toastNotif({
          text: data.message,
          color: '#da4848',
          timeout: 5000,
          icon: 'error'
        });
        $(':input[type="submit"]').prop('disabled', false);
      }
    },
    error: function() {}
  });
});


function edit_role(id) {
  $.ajax({
    url: '<?=base_url('Setting/role_edit_form')?>',
    type: 'POST',
    data: {id},
    dataType: 'html',
    success: function(data) {
      $('#editRoleModal').modal('show');
      $('#show_edit_data').html(data);
    }
  });
}


$("form#editRoleForm").submit(function(e) {
  $(':input[type="submit"]').prop('disabled', true);
  e.preventDefault();
  var formData = new FormData(this);
  $.ajax({
    url: $(this).attr('action'),
    type: 'POST',
    data: formData,
    cache: false,
    contentType: false,
    processData: false,
    dataType: 'json',
    success: function(data) {
      if (data.status == 200) {
        //$('.modal').modal('hide');
        toastNotif({
          text: data.message,
          color: '#5bc83f',
          timeout: 5000,
          icon: 'valid'
        });
        $(':input[type="submit"]').prop('disabled', false);
        setTimeout(function() {

          location.href = "<?=base_url('role')?>";

        }, 1000)
      } else if (data.status == 403) {
        toastNotif({
          text: data.message,
          color: '#da4848',
          timeout: 5000,
          icon: 'error'
        });

        $(':input[type="submit"]').prop('disabled', false);
      } else {
        toastNotif({
          text: data.message,
          color: '#da4848',
          timeout: 5000,
          icon: 'error'
        });
        $(':input[type="submit"]').prop('disabled', false);
      }
    },
    error: function() {}
  });
});


function view_role(id) {
  $.ajax({
    url: '<?=base_url('Setting/role_view_form')?>',
    type: 'POST',
    data: {id},
    dataType: 'html',
    success: function(data) {
      $('#viewRoleModal').modal('show');
      $('#show_view_data').html(data);
    }
  });
}


function closeCreateModal() {
  $('#RoleModal').modal('hide');
}

function closeEditModal() {
  $('#editRoleModal').modal('hide');
}

function closeViewModal() {
  $('#viewRoleModal').modal('hide');
}
</script>