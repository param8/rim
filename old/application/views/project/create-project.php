<div class="main-wrapper">
  <div class="page-wrapper">
    <div class="content container-fluid">
      <div class="page-header">
        <div class="row">
          <div class="col-md-12">
            <div class="card">
              <div class="card-header">
                <h4 class="card-title"><?=$page_title?></h4>
              </div>
              <div class="card-body pt-0">
                <form id="createProjectform" method="post" action="<?=base_url('Project/store');?>"
                  enctype="multipart/form-data">
                  <div class="settings-form row">

                    <div class="col-md-6">
                      <div class="form-group">
                        <label>Project Name<span class="star-red">*</span></label>
                        <input type="text" class="form-control" name="name" id="name" placeholder="Project Name">
                      </div>
                    </div>

                    <div class="col-md-6">
                      <div class="form-group">
                        <label>Client Name<span class="star-red">*</span></label>
                        <select class="form-control" name="clientID" id="clientID">
                          <option value="">Select Client</option>
                          <?php foreach($users as $user){?>
                            <option value="<?=$user->id?>"><?=$user->name?></option>
                            <?php } ?>
                        </select>
                        <!-- <input type="text" class="form-control" name="contact_name" id="contact_name" placeholder="Contact Name"> -->
                      </div>
                    </div>

                    <!-- <div class="col-md-6">
                      <div class="form-group">
                        <label>Contact Name<span class="star-red">*</span></label>
                        <input type="text" class="form-control" name="contact_name" id="contact_name" placeholder="Contact Name">
                      </div>
                    </div>

                    <div class="col-md-6">
                      <div class="form-group">
                        <label>Contact No.<span class="star-red">*</span></label>
                        <input type="text" class="form-control" name="contact_no" id="contact_no" minlength="10" maxlength="10" placeholder="Contact No" oninput="this.value = this.value.replace(/[^0-9]/g, '').replace(/(\..*)\./g, '$1');">
                      </div>
                    </div>

                    <div class="col-md-6">
                      <div class="form-group">
                        <label>Loctaion/Address<span class="star-red">*</span></label>
                        <textarea type="text" class="form-control" name="contact_address" id="contact_address" placeholder="Loctaion/Address"></textarea>
                      </div>
                    </div> -->
                    <div class="col-md-12">
                      <p class="settings-label">Add Chainage </p>
                      <div class="row form-row">
                        <div class="col-6 col-md-6">
                          <p class="settings-label">Start Chainage </p>
                        </div>
                        <div class="col-3 col-md-3">
                          <p class="settings-label">End Chainage </p>
                        </div>
                      </div>
                      <div class="chainage-form">
                        <div class="links-info">
                          <div class="row form-row links-cont">
                            <div class="col-6 col-md-6">
                              <div class="form-group form-placeholder d-flex">
                                <input type="text" class="form-control" name="start_chainage" id="start_chainage"
                                  onkeyup="get_chainage_value()" placeholder="Start Chainage">
                              </div>
                            </div>
                            <div class="col-6 col-md-6">
                              <div class="form-group form-placeholder d-flex">
                                <input type="text" class="form-control" name="end_chainage" id="end_chainage"
                                  onkeyup="get_chainage_value()" placeholder="End Chainage">
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="chainage-form" id="getShowProduct">
                      
                      </div>
                    </div>
                    <div class="form-group mb-0">
                      <div class="settings-btns">
                        <button type="submit" class="btn btn-orange">Submit</button>
                        <!-- <button type="reset" class="btn btn-grey">Cancel</button> -->
                      </div>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<script>
$("form#createProjectform").submit(function(e) {
  $(':input[type="submit"]').prop('disabled', true);
  e.preventDefault();
  var formData = new FormData(this);
  $.ajax({
    url: $(this).attr('action'),
    type: 'POST',
    data: formData,
    cache: false,
    contentType: false,
    processData: false,
    dataType: 'json',
    success: function(data) {
      if (data.status == 200) {
        //$('.modal').modal('hide');
        toastNotif({
          text: data.message,
          color: '#5bc83f',
          timeout: 5000,
          icon: 'valid'
        });
        $(':input[type="submit"]').prop('disabled', false);
        setTimeout(function() {

          location.href = "<?=base_url('project')?>";

        }, 1000)
      } else if (data.status == 403) {
        toastNotif({
          text: data.message,
          color: '#da4848',
          timeout: 5000,
          icon: 'error'
        });

        $(':input[type="submit"]').prop('disabled', false);
      } else {
        toastNotif({
          text: data.message,
          color: '#da4848',
          timeout: 5000,
          icon: 'error'
        });
        $(':input[type="submit"]').prop('disabled', false);
      }
    },
    error: function() {}
  });
});

function get_chainage_value() {
  var start_chainage = $('#start_chainage').val();
  var end_chainage = $('#end_chainage').val();
  $.ajax({
    url: '<?=base_url('Ajax_controller/get_chainage_value')?>',
    type: 'POST',
    data: {
      start_chainage,
      end_chainage
    },
    success: function(data) {
      $('#getShowProduct').html(data);
    }
  });
}
</script>