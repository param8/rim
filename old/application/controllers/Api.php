<?php
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set("Asia/Kolkata");
class Api extends MY_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->helper('verify_auth_token');
		$this->load->model("user_model");
		$this->load->model("api_model");
		$this->load->model("project_model");
	  header("Access-Control-Allow-Origin: *");
		header("Access-Control-Allow-Methods: GET, OPTIONS, POST, GET, PUT");
		header("Access-Control-Allow-Headers: Content-Type, Content-Length, Accept-Encoding");
	}

	public function login(){
		$jwt = new JWT();

		$JwtSecretKey = "e10adc3949ba59abbe56e057f20f883e";

		$email = $this->input->post('email');
		$password = $this->input->post('password');
		
		if(empty($email)){
			$error = array(
				"status"=>404,
				"message"=>"Please enter email",
				"sucess"=>false
				);

			echo json_encode($error);
			exit();
     }
     
     if(empty($password)){
			$error = array(
				"status"=>404,
				"message"=>"Please enter password",
				"sucess"=>false
				);

			echo json_encode($error);
			exit();
     }

		$result = $this->api_model->login($email,$password);
       
		if($result===403 || empty($result)){

			$error = array(
				"status"=>404,
				"message"=>"Email or password is wrong",
				);

			echo json_encode($error);
		}else{
			$token = $jwt->encode($result,$JwtSecretKey,'HS256');
			
			$success = array(
				"status"=>200,
				"message"=>'Login Successfully',
				"token"=>$token
				);

			echo json_encode($success);
			
		}

	}

	// public function signup(){

	// 	if($this->input->post()){
	// 		$name = $this->input->post('name');
	// 		$email = $this->input->post('email');
	// 		$password = $this->input->post('password');
	// 		$role_id = $this->input->post('role_id');
	// 		$data = array(
	// 			'name'=>$name,
	// 			'email'=>$email ,
	// 			'password'=> $password,
	// 			'role_id'=> $role_id,
	// 		);

	// 		$userId = $this->AuthModel->signup($data);
	// 		if($userId){
	// 			echo 'User Registered successfully!';
	// 		}else{
	// 			echo 'User Registeration faild!';
	// 		}
	// 	}

	// }


	public function road_inspection(){
		
		$headerToken = $this->input->get_request_header('Authorization');
		
		$splitToken = explode(" ", $headerToken);
		$token =  $splitToken[1];
			try {
				$token = verifyAuthToken($token);
				if($token){
					$userID = $this->input->post('userID');
					$projectID = $this->input->post('projectID');
					$location = $this->input->post('location');
					$chainage = $this->input->post('chainage');
					$inspection_date = $this->input->post('inspection_date');
					$inspection_time = $this->input->post('inspection_time');
					$work_perform = $this->input->post('work_perform');
					$note = $this->input->post('note');
					$inspector_name = $this->input->post('inspector_name');


    if(empty($projectID)){
			$error = array(
				"status"=>404,
				"message"=>"Please select project",
				"sucess"=>false
				);

			echo json_encode($error);
			exit();
     }

     if(empty($location)){
			$error = array(
				"status"=>404,
				"message"=>"Please enter a location",
				"sucess"=>false
				);

			echo json_encode($error);
			exit();
     }

     if(empty($chainage)){
			$error = array(
				"status"=>404,
				"message"=>"Please select chainage",
				"sucess"=>false
				);

			echo json_encode($error);
			exit();
     }

     if(empty($inspection_date)){
			$error = array(
				"status"=>404,
				"message"=>"Please enter inspection date",
				"sucess"=>false
				);

			echo json_encode($error);
			exit();
     }

     if(empty($inspection_time)){
			$error = array(
				"status"=>404,
				"message"=>"Please enter inspection time",
				"sucess"=>false
				);

			echo json_encode($error);
			exit();
     }

     if(empty($work_perform)){
			$error = array(
				"status"=>404,
				"message"=>"Please  select work performed",
				"sucess"=>false
				);

			echo json_encode($error);
			exit();
     }

     if(empty($note)){
			$error = array(
				"status"=>404,
				"message"=>"Please  enter note",
				"sucess"=>false
				);

			echo json_encode($error);
			exit();
     }

     if(empty($inspector_name)){
			$error = array(
				"status"=>404,
				"message"=>"Please  enter inspector name",
				"sucess"=>false
				);

			echo json_encode($error);
			exit();
     }
		 //$image = $this->input->post('image');
		$this->load->library('upload');
		if($_FILES['image']['name'] != '')
			{
		$config = array(
			'upload_path' 	=> 'uploads/project_inspection',
			'file_name' 	=> uniqid(),
			'allowed_types' => 'jpg|jpeg|png|gif|webp',
			'max_size' 		=> '10000000',
		);
			$this->upload->initialize($config);
		if ( ! $this->upload->do_upload('image'))
			{
				$error = $this->upload->display_errors();
				echo json_encode(['status'=>403, 'message'=>$error]);
				exit();
			}
			else
			{
			$type = explode('.',$_FILES['image']['name']);
			$type = $type[count($type) - 1];
			$image = 'uploads/project_inspection/'.$config['file_name'].'.'.$type;
			}
		}else{
		
          $error = array(
				"status"=>404,
				"message"=>"Please  upload image",
				"sucess"=>false
				);

			echo json_encode($error);
			exit();
		}
	

		$data = array(
      'userID'          => $userID,
			'projectID'       => $projectID,
			'location'        => $location,
			'chainage'        => $chainage,
			'inspection_date' => date('Y-m-d',strtotime($inspection_date)),
			'inspection_time' => $inspection_time,
			'work_perform'     => $work_perform,
			'note'            => $note,
			'image'           => $image,
      'inspector_name'  => $inspector_name,
		);

		$storeID = $this->project_model->store_road_inspection_maintenance($data);
    
    if($storeID){
			$data_history = array(
				'inspectionID'    => $storeID,
				'userID'          => $userID,
				'projectID'       => $projectID,
				'location'        => $location,
				'chainage'        => $chainage,
				'inspection_date' => date('Y-m-d',strtotime($inspection_date)),
				'inspection_time' => $inspection_time,
				'work_perform'    => $work_perform,
				'note'            => $note,
				'image'           => $image,
				'inspector_name'  => $inspector_name,
			);
	
			$store_history = $this->project_model->store_road_inspection_history($data_history);
			$success = array(
				"status"=>200,
				"message"=>"Road inspection created successfully",
				"sucess"=>true
				);

			echo json_encode($success);
			exit();
     }else{
			$error = array(
				"status"=>404,
				"message"=>"Something went wrong",
				"sucess"=>false
				);

			echo json_encode($error);
			exit();
     }
				}
					
			}
			catch(Exception $e) {
			// echo 'Message: ' .$e->getMessage();
				$error = array(
					"status"=>404,
					"message"=>"Invalid Token provided",
					"sucess"=>false
					);
	
				echo json_encode($error);
			}
			
			
		}



	public function projects(){

	$headerToken = $this->input->get_request_header('Authorization', TRUE);
	$splitToken = explode(" ", $headerToken);
	$token =  $splitToken[1];
	try {
		$token = verifyAuthToken($token);
		if($token){
			$projects=$this->project_model->get_projects(array('status'=>1));
			echo json_encode($projects);
		}
			
	}
	catch(Exception $e) {
	// echo 'Message: ' .$e->getMessage();
		$error = array(
			"status"=>404,
			"message"=>"Invalid Token provided",
			"sucess"=>false
			);

		echo json_encode($error);
	}
		
		
	}

	public function chainages(){
		
		$headerToken = $this->input->get_request_header('Authorization');
		
		$splitToken = explode(" ", $headerToken);
		$token =  $splitToken[1];
			try {
				$token = verifyAuthToken($token);
				if($token){
					$id = $this->input->post('project_id');
           $chainages=$this->project_model->get_chainages(array('projectID'=>$id,'status'=>1));
					echo json_encode($chainages);
				}
					
			}
			catch(Exception $e) {
			// echo 'Message: ' .$e->getMessage();
				$error = array(
					"status"=>404,
					"message"=>"Invalid Token provided",
					"sucess"=>false
					);
	
				echo json_encode($error);
			}
		}

		public function work_perform(){
			$headerToken = $this->input->get_request_header('Authorization');
			$splitToken = explode(" ", $headerToken);
			$token =  $splitToken[1];
				try {
					$token = verifyAuthToken($token);
					if($token){
						$work_performs=$this->project_model->get_observations(array('status'=>1));
						echo json_encode($work_performs);
					}		
				}
				catch(Exception $e) {
				// echo 'Message: ' .$e->getMessage();
					$error = array(
						"status"=>404,
						"message"=>"Invalid Token provided",
						"sucess"=>false
						);
		
					echo json_encode($error);
				}
			}

}