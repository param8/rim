<?php 
class Authantication extends MY_Controller 
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('Auth_model');
		$this->load->model('User_model');
        $this->load->model('setting_model');
	}


	public function index()
	{	
		$data['page_title'] = 'Login';
		$this->login_template('signin',$data);
		// $this->load->view('layout/login_head',$data);
	}

	public function user_login()
	{	
		$data['page_title'] = 'Login';
		if($this->session->userdata('email')){
			redirect(base_url('home'));
		}
		$this->template('web/login',$data);
		// $this->load->view('layout/login_head',$data);
	}

	public function login(){
		$email = $this->input->post('email');
		$password = $this->input->post('password');
		if(empty($email)){
			echo json_encode(['status'=>403, 'message'=>'Please enter your email or contact']); 	
			exit();
		}
		if(empty($password)){
			echo json_encode(['status'=>403, 'message'=>'Please enter your password']); 	
			exit();
		}
		$login = $this->Auth_model->login($email,$password);
		if($login==403){
			echo json_encode(['status'=>403, 'message'=>'email or password incorrect!']);
		}elseif($login==302){
			echo json_encode(['status'=>302, 'message'=>'Your are not active please contact the administrator']);   
		}else{
			echo json_encode(['status'=>200, 'message'=>'Login Successfully','user_type'=>$this->session->userdata('user_type'),'verifivation_status'=>$this->session->userdata('verification_status')]);
			// redirect(base_url('dashboard'), 'refresh');
    }
	}


	public function create_user(){
		$menuID = 2;
		$permission = $this->permissions()->$menuID;
		if($permission[1]=='Add'){
      $data['page_title'] = 'New User';
		  $this->admin_template('users/create',$data);
	  }else{
	  	redirect(base_url('dashboard'));
	  }
	}

	public function register(){
		if($this->session->userdata('email')){
			redirect(base_url('home'));
		}
    $data['page_title'] = 'Registration';
		$data['states'] = $this->Common_model->get_states(array('country_id'=>101));
		$this->template('web/register',$data);
	}


	public function store(){
		$name = $this->input->post('name');
		$email = $this->input->post('email');
		$phone = $this->input->post('phone');
		$address = $this->input->post('address');
		$user_type = $this->input->post('user_type');
		$profile_pic = 'public/dummy_user.png';	
		$created_by = $this->input->post('created_by');
		$validate_email = $this->User_model->get_user(array('users.email' => $email,'users.status'=>1));
		$validate_phone = $this->User_model->get_user(array('users.phone' => $phone,'users.status'=>1));
		$password = $phone;
   if(empty($name)){
			echo json_encode(['status'=>403, 'message'=>'Please enter name']); 	
			exit();
		}
		if(empty($email)){
			echo json_encode(['status'=>403, 'message'=>'Please enter your email ']); 	
			exit();
		}
    
		if($validate_email){
			echo json_encode(['status'=>403, 'message'=>'This email address is already in use.']); 	
			exit();
		}

		if(empty($phone)){
			echo json_encode(['status'=>403, 'message'=>'Please enter your phone ']); 	
			exit();
		}
   
		if($validate_phone){
			echo json_encode(['status'=>403, 'message'=>'This phone number is already in use.']); 	
			exit();
		}

		if(empty($address)){
			echo json_encode(['status'=>403, 'message'=>'Please enter address ']); 	
			exit();
		}

		if(empty($user_type)){
			echo json_encode(['status'=>403, 'message'=>'Please select role ']); 	
			exit();
		}

		$menus = $this->menus();
    $permission = array();
		foreach($menus as $menu){
			if($menu->child_name=='Dashboard'){
				$permission[$menu->id] = array('View','Add','Edit','Delete','Upload','Export');
			}else{
			$view = !empty($this->input->post('view')[$menu->id]) ? $this->input->post('view')[$menu->id] : '';
			$add = !empty($this->input->post('add')[$menu->id]) ? $this->input->post('add')[$menu->id] : '';
			$edit = !empty($this->input->post('edit')[$menu->id]) ? $this->input->post('edit')[$menu->id] : '';
			$delete = !empty($this->input->post('delete')[$menu->id]) ? $this->input->post('delete')[$menu->id] : '';
			$upload = !empty($this->input->post('upload')[$menu->id]) ? $this->input->post('upload')[$menu->id] : '';
			$export = !empty($this->input->post('export')[$menu->id]) ? $this->input->post('export')[$menu->id] : '';
			$permission[$menu->id] = array($view,$add,$edit,$delete,$upload,$export);
			}
		}
		$permission_json = json_encode($permission);
	
		$unique_id =  uniqid("VMAKS".date('dmYHis'));
		$data = array(	
			'adminID'      => $this->session->userdata('id'),      
			'unique_id'    => $unique_id,
			'name'         => $name,
			'email'        => $email,
			'phone'        => $phone,
			'address'      => $address,
			'profile_pic'  => $profile_pic,
			'user_type'    => $user_type,
			'password'     => md5($password),
			'permission'   => $permission_json
		);

    $user_id = $this->User_model->store_user($data);
		if($user_id){
		 echo json_encode(['status'=>200, 'message'=>'User Registration Successfully']);   
		}else{
      echo json_encode(['status'=>403, 'message'=>mysqli_error()]);
    }

	}

	public function logout()
	{
		$this->session->sess_destroy();
		redirect(base_url(), 'refresh');
	}

//   Admin Cradinciales

	
}