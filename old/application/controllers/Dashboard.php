<?php 
class Dashboard extends MY_Controller 
{
	public function __construct()
	{
		parent::__construct();
		$this->not_admin_logged_in();
		$this->load->model('auth_model');
		$this->load->model('user_model');
		$this->load->model('project_model');
	}

	public function index()
	{	 
		$data['page_title'] = 'Dashboard';
		$data['admin'] = $this->user_model->get_users(array('users.status'=>1,'roles.name'=>'Admin'));
		$data['user'] = $this->user_model->get_users(array('users.status'=>1,'roles.name'=>'User'));
		$data['client'] = $this->user_model->get_users(array('users.status'=>1,'roles.name'=>'Client'));
		$data['projects'] = $this->project_model->get_projects(array('project.status'=>1));
		$data['work_performed'] = $this->project_model->get_observations(array('status'=>1));
		$data['road_inspection'] = $this->project_model->get_road_inspections(array('road_inspetion_maintenance.status'=>1));
		$data['charts'] = $this->get_chart($data['projects']);
	  $this->admin_template('dashboard',$data);
		
	}


	public function get_chart($projects){
    
		$total_process = array();
		foreach($projects as $project){
			$this->db->select('road_inspetion_maintenance.*,work_perform.name as defectName');
			$this->db->join('work_perform','work_perform.id=road_inspetion_maintenance.work_perform','left' );
			$this->db->where('road_inspetion_maintenance.projectID',$project->id);
			$road = $this->db->get('road_inspetion_maintenance')->result();
			if(count($road)>0){
				foreach($road as $key=>$data){
					$total_defet = $data->work_perform;
					$total_process[$project->id] = "['Road Inspection', 'Work On'],["."'".$data->defectName."'".", 1]";
				 }
			}else{
				$total_process[$project->id] = "['Road Inspection', 'Work On'],['Defect Not Use', 0]";
			}
			 
			 
 		}
		return $total_process;
    print_r($total_process);die;
	}



}