<?php
class Common extends MY_Controller 
{
    public function __construct()
    {
      parent::__construct();
     $this->load->model('setting_model');
     $this->load->model('course_model');
     $this->load->library('csvimport');
    }

    public function index(){
      $data['page_title'] = 'General-Setting';
      $this->admin_template('components/breadcrumb',$data);
      $this->admin_template('setting/setting',$data);
    }

    
    
    public function store_terms(){
      $terms = $this->input->post('terms');
       if(empty($terms)){
        echo json_encode(['status'=>403, 'message'=>'Please Enter Terms Conditions']); 	
        exit();
       }

      $data = array(
        'terms_of_use' => $terms,
       );
    // print_r($data);die;
       $update = $this->setting_model->update_siteInfo($data);
 
       if($update){
         echo json_encode(['status'=>200, 'message'=>'Terms Conditions updated successfully!']);
        }else{
            echo json_encode(['status'=>302, 'message'=>'Something wrong happened']);   
        }
 
    }

    public function store_privacy(){
      //print_r($_POST);die;
      $privacy = $this->input->post('privacy');
       if(empty($privacy)){
        echo json_encode(['status'=>403, 'message'=>'Please Enter Privacy Policy']); 	
        exit();
       }

      $data = array(
        'privacy_policy' => $privacy,
       );
    // print_r($data);die;
       $update = $this->setting_model->update_siteInfo($data);
 
       if($update){
         echo json_encode(['status'=>200, 'message'=>'Privacy Policy updated successfully!']);
        }else{
            echo json_encode(['status'=>302, 'message'=>'Something wrong happened']);   
        }
    }

    public function store_refund(){
      $refund_policy = $this->input->post('refund');
       if(empty($refund_policy)){
        echo json_encode(['status'=>403, 'message'=>'Please Enter Refund Policy']); 	
        exit();
       }

      $data = array(
        'refund_policy' => $refund_policy,
       );
    // print_r($data);die;
       $update = $this->setting_model->update_siteInfo($data);
 
       if($update){
         echo json_encode(['status'=>200, 'message'=>'Refund Policy updated successfully!']);
        }else{
            echo json_encode(['status'=>302, 'message'=>'Something wrong happened']);   
        }
    }

    public function store_payment(){
      $razor_key = $this->input->post('razor_key');
      $status = ($this->input->post('status') !='')?1:0;
      $paymnt_info = $this->Common_model->get_payment_info();
      $this->load->library('upload');
      if(!empty($_FILES['payment_logo']['name'])){
       $config = array(
        'upload_path' 	=> 'uploads/payment',
        'file_name' 	=> str_replace(' ','','razor_logo').uniqid(),
        'allowed_types' => 'jpg|jpeg|png|gif',
        'max_size' 		=> '10000000',
       );
       $this->upload->initialize($config);
      if ( ! $this->upload->do_upload('payment_logo'))
        {
            $error = $this->upload->display_errors();
            echo json_encode(['status'=>403, 'message'=>$error]); 	
            exit();
        }
        else
        {
          $type = explode('.', $_FILES['payment_logo']['name']);
          $type = $type[count($type) - 1];
          $image = 'uploads/payment/'.$config['file_name'].'.'.$type;
        }
      }elseif(!empty($paymnt_info->payment_logo)){
        $image = $paymnt_info->payment_logo;
      }else{
        $image = 'public/website/images/dummy_image.jpg';
      }
      $data = array(
        'razopay_key' => $razor_key,
        'payment_logo' => $image,
        'status' => $status,
       );
    // print_r($data);die;
       $update = $this->setting_model->update_paymentInfo($data);
 
       if($update){
         echo json_encode(['status'=>200, 'message'=>'Payment-Setting update successfully!']);
        }else{
            echo json_encode(['status'=>302, 'message'=>'Something wrong happened']);   
        }
 
    }

    public function store_socialmedia(){
      $facebook_url = $this->input->post('facebook');
      $insta_url = $this->input->post('instagram');
      $twitter_url = $this->input->post('twitter');
      $linkedin_url = $this->input->post('linkedin');
      $youtube_url = $this->input->post('youtube');

      $siteinfo = $this->Common_model->get_site_info();

      $data = array(
        'facebook_url' => $facebook_url,
        'insta_url' => $insta_url,
        'twitter_url' => $twitter_url,
        'linkedin_url' => $linkedin_url,
        'youtube_url' => $youtube_url, 
       );
    // print_r($data);die;
       $update = $this->setting_model->update_siteInfo($data);
 
       if($update){
         echo json_encode(['status'=>200, 'message'=>'Site-info update successfully!']);
        }else{
            echo json_encode(['status'=>302, 'message'=>'Something wrong happened']);   
        }
 

    }

    public function store_seo_form(){
      $title = $this->input->post('meta_title');
      $alt_text = $this->input->post('img_alt_text');
      $keyword = $this->input->post('keywords');
      $description = $this->input->post('description');
      if(empty($title)){
        echo json_encode(['status'=>402, 'message'=>'Please Enter Meta Title']);  
      }
      if(empty($alt_text)){
        echo json_encode(['status'=>402, 'message'=>'Please Enter Image Alt Text']);  
      }
      if(empty($keyword)){
        echo json_encode(['status'=>402, 'message'=>'Please Enter Keywords']);  
      }
      if(empty($description)){
        echo json_encode(['status'=>402, 'message'=>'Please Enter Description']);  
      }
      $data = array(
        'meta_title' => $title,
        'img_alt_text' => $alt_text,
        'meta_keywords' => $keyword,
        'meta_description' => $description,
       );
    // print_r($data);die;
       $update = $this->setting_model->update_seoInfo($data);
 
       if($update){
         echo json_encode(['status'=>200, 'message'=>'Site-info update successfully!']);
        }else{
            echo json_encode(['status'=>302, 'message'=>'Something wrong happened']);   
        }
    }

  public function store_email_form(){
    $email_address = $this->input->post('from_address');
    $username = $this->input->post('username');
    $password = $this->input->post('password');
    $from_name = $this->input->post('from_name');    
    $host = $this->input->post('host');
    $port = $this->input->post('port');
    $status = ($this->input->post('status') !='')?1:0;
    //$status = $this->input->post['status'];

    $emailinfo = $this->Common_model->get_email_info();

      $data = array(
        'email_address' => $email_address,
        'email_host' => $host,
        'email_port' => $port,
        'email_name' => $from_name,
        'user_name' => $username, 
        'email_pswrd' => $password,
        'status' => $status,
       );
     //print_r($data);die;
       $update = $this->setting_model->update_email_info($data);
 
       if($update){
         echo json_encode(['status'=>200, 'message'=>'Email-setting update successfully!']);
        }else{
            echo json_encode(['status'=>302, 'message'=>'Something wrong happened']);   
        }
   }

  public function current_affairs() {
    $this->not_admin_logged_in();
    $data['page_title'] = 'Current Affairs';
    $this->admin_template('setting/current-affairs',$data);
  }

  public function ajaxCurrentAffires(){
    $this->not_admin_logged_in();
		$condition = array('status'=>1);
		$affairs = $this->setting_model->make_datatables_affairs($condition); // this will call modal function for fetching data
		$data = array();
		foreach($affairs as $key=>$affair) // Loop over the data fetched and store them in array
		{
    $button = '';
			$sub_array = array();
      $button .= '<a href="'.base_url('edit-current-affairs/'.base64_encode($affair['id'])).'"  data-bs-toggle="tooltip" data-bs-placement="bottom" title="Update current affiares" class="btn btn-sm bg-primary-light"><i class="fe fe-edit"></i> </a>';
      $button .= '<a href="javascript:void(0)" onclick="delete_current_affairs('.$affair['id'].')" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Delete current affiares" class="btn btn-sm bg-danger-light"><i class="fe fe-trash"></i> </a>';
			
			$sub_array[] = $key+1;
			$sub_array[] = $affair['title'];
			$sub_array[] = $affair['description'];
			$sub_array[] = date('d-F-Y', strtotime($affair['created_at']));
			$sub_array[] = $button;
		  $data[] = $sub_array;
		}
	
		$output = array(
			"draw"                    =>     intval($_POST["draw"]),
			"recordsTotal"            =>     $this->setting_model->get_all_data_affairs($condition),
			"recordsFiltered"         =>     $this->setting_model->get_filtered_data_affairs($condition),
			"data"                    =>     $data
		);
		
		echo json_encode($output);
  }

  public function create_current_affairs(){
    $data['page_title'] = 'Create Current Affairs';
    $this->admin_template('setting/create-current-affairs',$data);
  }

  public function edit_current_affairs(){
    $data['page_title'] = 'Edit Current Affairs';
    $id = base64_decode($this->uri->segment(2));
    $data['affair'] = $this->setting_model->get_current_affairs(array('id' => $id));
    $this->admin_template('setting/edit-current-affairs',$data);
  }

  public function store_current_affairs(){
    $title = $this->input->post('title');
    $description = $this->input->post('description');
    if(empty($title)){
      echo json_encode(['status'=>402, 'message'=>'Please enter a title']);
      exit();
     }

     if(empty($description)){
      echo json_encode(['status'=>402, 'message'=>'Please enter a description']);
      exit();
     }

     $data = array(
      'title'       => $title,
      'description' => $description
     );

     $store = $this->setting_model->store_current_affairs($data);

    if($store){
      echo json_encode(['status'=>200, 'message'=>'Current Affairs added successfully!']);
     }else{
         echo json_encode(['status'=>302, 'message'=>'Something wrong happened']);   
     }
  }

  public function update_current_affairs(){
    $id = $this->input->post('id');
    $title = $this->input->post('title');
    $description = $this->input->post('description');
    if(empty($title)){
      echo json_encode(['status'=>402, 'message'=>'Please enter a title']);
      exit();
     }

     if(empty($description)){
      echo json_encode(['status'=>402, 'message'=>'Please enter a description']);
      exit();
     }

     $data = array(
      'title'       => $title,
      'description' => $description
     );

     $update = $this->setting_model->update_current_affairs($data,$id);

    if($update){
      echo json_encode(['status'=>200, 'message'=>'Current Affairs updated successfully!']);
     }else{
         echo json_encode(['status'=>302, 'message'=>'Something wrong happened']);   
     }
  }

  public function delete_current_affairs(){
    $id = $this->input->post('id');

    $delete = $this->setting_model->delete_current_affairs($id);

    if($delete){
      echo json_encode(['status'=>200, 'message'=>'Current Affairs deleted successfully!']);
     }else{
         echo json_encode(['status'=>302, 'message'=>'Something wrong happened']);   
     }

  }


  public function whats_news() {
    $this->not_admin_logged_in();
    $data['page_title'] = "What's News";
    $this->admin_template('setting/whats-news',$data);
  }

  public function ajaxWhatsNews(){
    $this->not_admin_logged_in();
		$condition = array('status'=>1);
		$whatsNews = $this->setting_model->make_datatables_whatsNews($condition); // this will call modal function for fetching data
		$data = array();
		foreach($whatsNews as $key=>$whatsNew) // Loop over the data fetched and store them in array
		{
    $button = '';
			$sub_array = array();
      $button .= '<a href="'.base_url('edit-whats-news/'.base64_encode($whatsNew['id'])).'"  data-bs-toggle="tooltip" data-bs-placement="bottom" title="Update whats news" class="btn btn-sm bg-primary-light"><i class="fe fe-edit"></i> </a>';
      $button .= '<a href="javascript:void(0)" onclick="delete_whats_news('.$whatsNew['id'].')" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Delete whats news" class="btn btn-sm bg-danger-light"><i class="fe fe-trash"></i> </a>';
			
			$sub_array[] = $key+1;
			$sub_array[] = $whatsNew['title'];
			$sub_array[] = $whatsNew['description'];
			$sub_array[] = date('d-F-Y', strtotime($whatsNew['created_at']));
			$sub_array[] = $button;
		  $data[] = $sub_array;
		}
	
		$output = array(
			"draw"                    =>     intval($_POST["draw"]),
			"recordsTotal"            =>     $this->setting_model->get_all_data_whatsNews($condition),
			"recordsFiltered"         =>     $this->setting_model->get_filtered_data_whatsNews($condition),
			"data"                    =>     $data
		);
		
		echo json_encode($output);
  }

  public function create_whats_news(){
    $data['page_title'] = 'Create Whats News';
    $this->admin_template('setting/create-whats-news',$data);
  }

  public function edit_whats_news(){
    $data['page_title'] = 'Edit Whats News';
    $id = base64_decode($this->uri->segment(2));
    $data['news'] = $this->setting_model->get_whats_news(array('id' => $id));
    $this->admin_template('setting/edit-whats-news',$data);
  }

  public function store_whats_news(){
    $title = $this->input->post('title');
    $description = $this->input->post('description');
    if(empty($title)){
      echo json_encode(['status'=>402, 'message'=>'Please enter a title']);
      exit();
     }

     if(empty($description)){
      echo json_encode(['status'=>402, 'message'=>'Please enter a description']);
      exit();
     }

     $data = array(
      'title'       => $title,
      'description' => $description
     );

     $store = $this->setting_model->store_whats_news($data);

    if($store){
      echo json_encode(['status'=>200, 'message'=>'Whats News added successfully!']);
     }else{
         echo json_encode(['status'=>302, 'message'=>'Something wrong happened']);   
     }
  }

  public function update_whats_news(){
    $id = $this->input->post('id');
    $title = $this->input->post('title');
    $description = $this->input->post('description');
    if(empty($title)){
      echo json_encode(['status'=>402, 'message'=>'Please enter a title']);
      exit();
     }

     if(empty($description)){
      echo json_encode(['status'=>402, 'message'=>'Please enter a description']);
      exit();
     }

     $data = array(
      'title'       => $title,
      'description' => $description
     );

     $update = $this->setting_model->update_whats_news($data,$id);

    if($update){
      echo json_encode(['status'=>200, 'message'=>'Whats News updated successfully!']);
     }else{
         echo json_encode(['status'=>302, 'message'=>'Something wrong happened']);   
     }
  }


  public function delete_whats_news(){
    $id = $this->input->post('id');

    $delete = $this->setting_model->delete_whats_news($id);

    if($delete){
      echo json_encode(['status'=>200, 'message'=>'Whats News deleted successfully!']);
     }else{
         echo json_encode(['status'=>302, 'message'=>'Something wrong happened']);   
     }

  }

  public function videos() {
    $this->not_admin_logged_in();
    $data['page_title'] = "Videos";
    $this->admin_template('setting/videos',$data);
  }

  public function ajaxVideos(){
    $this->not_admin_logged_in();
		$condition = array('status'=>1);
		$videos = $this->setting_model->make_datatables_videos($condition); // this will call modal function for fetching data
		$data = array();
		foreach($videos as $key=>$video) // Loop over the data fetched and store them in array
		{
    $button = '';
			$sub_array = array();
      $button .= '<a href="'.base_url('edit-videos/'.base64_encode($video['id'])).'"  data-bs-toggle="tooltip" data-bs-placement="bottom" title="Edit Videos" class="btn btn-sm bg-primary-light"><i class="fe fe-edit"></i> </a>';
      $button .= '<a href="javascript:void(0)" onclick="delete_videos('.$video['id'].')" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Delete Videos" class="btn btn-sm bg-danger-light"><i class="fe fe-trash"></i> </a>';
			$video_ifram = '<iframe width="200" height="200" src="http://www.youtube.com/embed/'. $video['video'].'?autoplay=1" frameborder="0"></iframe>';
			$sub_array[] = $key+1;
			$sub_array[] = $video['title'];
			$sub_array[] = $video_ifram;
			$sub_array[] = date('d-F-Y', strtotime($video['created_at']));
			$sub_array[] = $button;
		  $data[] = $sub_array;
		}
	
		$output = array(
			"draw"                    =>     intval($_POST["draw"]),
			"recordsTotal"            =>     $this->setting_model->get_all_data_videos($condition),
			"recordsFiltered"         =>     $this->setting_model->get_filtered_data_videos($condition),
			"data"                    =>     $data
		);
		
		echo json_encode($output);
  }

  public function create_videos() {
    $this->not_admin_logged_in();
    $data['page_title'] = "Create Videos";
    $this->admin_template('setting/create-videos',$data);
  }

  public function store_videos(){
    $title = $this->input->post('title');
    $video = $this->input->post('video');
    if(empty($title)){
      echo json_encode(['status'=>402, 'message'=>'Please enter a title']);
      exit();
     }

     if(empty($video)){
      echo json_encode(['status'=>402, 'message'=>'Please enter a video link']);
      exit();
     }

     $data = array(
      'title'  => $title,
      'video'  => $video
     );

     $store = $this->setting_model->store_videos($data);

    if($store){
      echo json_encode(['status'=>200, 'message'=>'Videos added successfully!']);
     }else{
         echo json_encode(['status'=>302, 'message'=>'Something wrong happened']);   
     }
  }

  public function edit_videos() {
    $this->not_admin_logged_in();
    $id = base64_decode($this->uri->segment(2));
    $data['page_title'] = "Edit Videos";
    $data['video'] = $this->setting_model->get_video(array('id' => $id,'status' => 1));
    $this->admin_template('setting/edit-videos',$data);
  }

  public function update_videos(){
    $videoID = $this->input->post('videoID');
    $title = $this->input->post('title');
    $video = $this->input->post('video');
    if(empty($title)){
      echo json_encode(['status'=>402, 'message'=>'Please enter a title']);
      exit();
     }

     if(empty($video)){
      echo json_encode(['status'=>402, 'message'=>'Please enter a video link']);
      exit();
     }

     $data = array(
      'title'  => $title,
      'video'  => $video
     );

     $update = $this->setting_model->update_videos($data, $videoID);

    if($update){
      echo json_encode(['status'=>200, 'message'=>'Videos updates successfully!']);
     }else{
         echo json_encode(['status'=>302, 'message'=>'Something wrong happened']);   
     }
  }

  public function delete_videos(){
    $id = $this->input->post('id');

    $delete = $this->setting_model->delete_videos($id);

    if($delete){
      echo json_encode(['status'=>200, 'message'=>'Video deleted successfully!']);
     }else{
         echo json_encode(['status'=>302, 'message'=>'Something wrong happened']);   
     }

  }

  public function ajaxDiscount(){
    $this->not_admin_logged_in();
		$condition = array('course_discount.status'=>1);
		$discounts = $this->setting_model->make_datatables_discount($condition); // this will call modal function for fetching data
		$data = array();
		foreach($discounts as $key=>$discount) // Loop over the data fetched and store them in array
		{
    $button = '';
			$sub_array = array();
      $button .= '<a href="'.base_url('edit-discount/'.base64_encode($discount['id'])).'"  data-bs-toggle="tooltip" data-bs-placement="bottom" title="Edit Videos" class="btn btn-sm bg-primary-light"><i class="fe fe-edit"></i> </a>';
      $button .= '<a href="javascript:void(0)" onclick="delete_discount('.$discount['id'].')" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Delete Videos" class="btn btn-sm bg-danger-light"><i class="fe fe-trash"></i> </a>';
			$sub_array[] = $key+1;
			$sub_array[] = $discount['title'];
			$sub_array[] = $discount['coupon'];
      $sub_array[] = $discount['courseName'];
      $sub_array[] = $discount['discount_type'];
      $sub_array[] = $discount['discount_type']=='Percentage' ? '% '.$discount['discount'] : '₹ '.$discount['discount'];
      $sub_array[] = $discount['min_order'];
      $sub_array[] = !empty($discount['expiry_date']) ? date('d-m-Y', strtotime($discount['expiry_date'])) : '';
			$sub_array[] = date('d-F-Y', strtotime($discount['created_at']));
			$sub_array[] = $button;
		  $data[] = $sub_array;
		}
	
		$output = array(
			"draw"                    =>     intval($_POST["draw"]),
			"recordsTotal"            =>     $this->setting_model->get_all_data_discount($condition),
			"recordsFiltered"         =>     $this->setting_model->get_filtered_data_discount($condition),
			"data"                    =>     $data
		);
		
		echo json_encode($output);
  }

  public function discount(){
    $this->not_admin_logged_in();
    $data['page_title'] = "Discount";
    $data['courses'] = $this->course_model->get_courses(array('courses.status'=>1));
    $this->admin_template('setting/discount',$data);
  }

  public function create_discount(){
    $this->not_admin_logged_in();
    $data['page_title'] = "Create Discount";
    $data['courses'] = $this->course_model->get_courses(array('courses.status'=>1));
    $this->admin_template('setting/create-discount',$data);
  }



  public function store_discount(){
    $courseID = $this->input->post('courseID');
    $title = $this->input->post('title');
    $discount_type = $this->input->post('discount_type');
    $discount = $this->input->post('discount');
    $min_order = $this->input->post('min_order');
    $coupon = $this->input->post('coupon');
    $expiry_date = $this->input->post('expiry_date');

    if(empty($title)){
      echo json_encode(['status'=>402, 'message'=>'Please enter a title']);
      exit();
     }

    if(empty($courseID)){
      echo json_encode(['status'=>402, 'message'=>'Please select a course']);
      exit();
     }

 

     if(empty($discount_type)){
      echo json_encode(['status'=>402, 'message'=>'Please select a discount type']);
      exit();
     }

     if(empty($discount)){
      echo json_encode(['status'=>402, 'message'=>'Please enter a discount']);
      exit();
     }

     if(empty($coupon)){
      echo json_encode(['status'=>402, 'message'=>'Please enter a coupon']);
      exit();
     }
     $check_coupon = $this->setting_model->get_discount(array('course_discount.coupon'=>$coupon));
     if($check_coupon){
      echo json_encode(['status'=>402, 'message'=>'This discount coupon is already in use']);
      exit();
     }

     if(empty($expiry_date)){
      echo json_encode(['status'=>402, 'message'=>'Please enter a expiration date']);
      exit();
     }
   
     $data = array(
      'courseID' => $courseID,
      'title' => $title,
      'discount_type' => $discount_type,
      'discount' => $discount,
      'min_order' => $min_order,
      'coupon' => $coupon,
      'expiry_date' => $expiry_date,
     );

     $store = $this->setting_model->store_discount($data);

    if($store){
      echo json_encode(['status'=>200, 'message'=>'Discount Coupon added successfully!']);
     }else{
         echo json_encode(['status'=>302, 'message'=>'Something wrong happened']);   
     }

  }

  public function edit_discount(){
    $this->not_admin_logged_in();
    $data['page_title'] = "Edit Discount";
    $id = base64_decode($this->uri->segment(2));
    $data['discount'] = $this->setting_model->get_discount(array('course_discount.id'=>$id));
    $data['courses'] = $this->course_model->get_courses(array('courses.status'=>1));
    $this->admin_template('setting/edit-discount',$data);
  }

  public function update_discount(){
    $discountID = $this->input->post('discountID');
    $courseID = $this->input->post('courseID');
    $title = $this->input->post('title');
    $discount_type = $this->input->post('discount_type');
    $discount = $this->input->post('discount');
    $min_order = $this->input->post('min_order');
    $coupon = $this->input->post('coupon');
    $expiry_date = $this->input->post('expiry_date');

    if(empty($title)){
      echo json_encode(['status'=>402, 'message'=>'Please enter a title']);
      exit();
     }

    if(empty($courseID)){
      echo json_encode(['status'=>402, 'message'=>'Please select a course']);
      exit();
     }

 

     if(empty($discount_type)){
      echo json_encode(['status'=>402, 'message'=>'Please select a discount type']);
      exit();
     }

     if(empty($discount)){
      echo json_encode(['status'=>402, 'message'=>'Please enter a discount']);
      exit();
     }

     if(empty($coupon)){
      echo json_encode(['status'=>402, 'message'=>'Please enter a coupon']);
      exit();
     }
     $check_coupon = $this->setting_model->get_discount(array('course_discount.coupon'=>$coupon,'course_discount.id <>'=>$discountID));
     if($check_coupon){
      echo json_encode(['status'=>402, 'message'=>'This discount coupon is already in use']);
      exit();
     }

     if(empty($expiry_date)){
      echo json_encode(['status'=>402, 'message'=>'Please enter a expiration date']);
      exit();
     }
   
     $data = array(
      'courseID' => $courseID,
      'title' => $title,
      'discount_type' => $discount_type,
      'discount' => $discount,
      'min_order' => $min_order,
      'coupon' => $coupon,
      'expiry_date' => $expiry_date,
     );

     $update = $this->setting_model->update_discount($data,$discountID);

    if($update){
      echo json_encode(['status'=>200, 'message'=>'Discount Coupon updated successfully!']);
     }else{
         echo json_encode(['status'=>302, 'message'=>'Something wrong happened']);   
     }

  }

  public function delete_discount(){
    $id = $this->input->post('id');

    $delete = $this->setting_model->delete_discount($id);

    if($delete){
      echo json_encode(['status'=>200, 'message'=>'Discount deleted successfully!']);
     }else{
         echo json_encode(['status'=>302, 'message'=>'Something wrong happened']);   
     }
  }


  public function toppers() {
    $this->not_admin_logged_in();
    $data['page_title'] = "Toppers Talk";
    $this->admin_template('common/toppers',$data);
  }

  public function ajaxToppers(){
    $this->not_admin_logged_in();
		$condition = array('status'=>1);
		$toppers = $this->setting_model->make_datatables_toppers($condition); // this will call modal function for fetching data
		$data = array();
		foreach($toppers as $key=>$topper) // Loop over the data fetched and store them in array
		{
    $button = '';
			$sub_array = array();
      $button .= '<a href="'.base_url('edit-toppers/'.base64_encode($topper['id'])).'"  data-bs-toggle="tooltip" data-bs-placement="bottom" title="Edit Toppers" class="btn btn-sm bg-primary-light"><i class="fe fe-edit"></i> </a>';
      $button .= '<a href="javascript:void(0)" onclick="delete_toppers('.$topper['id'].')" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Delete Toppers" class="btn btn-sm bg-danger-light"><i class="fe fe-trash"></i> </a>';
			$image = '<img src="'.base_url($topper['image']).'" style="width:100px;height:100px"> ';
			$sub_array[] = $key+1;
      $sub_array[] = $image;
			$sub_array[] = $topper['title'];
			$sub_array[] = $topper['video_link'];
			$sub_array[] = date('d-F-Y', strtotime($topper['created_at']));
			$sub_array[] = $button;
		  $data[] = $sub_array;
		}
	
		$output = array(
			"draw"                    =>     intval($_POST["draw"]),
			"recordsTotal"            =>     $this->setting_model->get_all_data_toppers($condition),
			"recordsFiltered"         =>     $this->setting_model->get_filtered_data_toppers($condition),
			"data"                    =>     $data
		);
		
		echo json_encode($output);
  }

  public function create_toppers() {
    $this->not_admin_logged_in();
    $data['page_title'] = "Create Topper Talk";
    $this->admin_template('common/create-toppers',$data);
  }

  public function store_toppers(){
    $title = $this->input->post('title');
    $video_link = $this->input->post('video_link');
    $slug = str_replace(' ','-',strtolower($title));
    if(empty($title)){
      echo json_encode(['status'=>402, 'message'=>'Please enter a title']);
      exit();
     }

     $this->load->library('upload');
    if($_FILES['image']['name'] != '')
		{
    $config = array(
      'upload_path' 	=> 'uploads/toppers_talk',
      'file_name' 	=> str_replace(' ','',$name).uniqid(),
      'allowed_types' => 'jpg|jpeg|png|gif|webp',
      'max_size' 		=> '10000000',
    );
        $this->upload->initialize($config);
    if ( ! $this->upload->do_upload('image'))
      {
          $error = $this->upload->display_errors();
          echo json_encode(['status'=>403, 'message'=>$error]);
          exit();
      }
      else
      {
        $type = explode('.',$_FILES['image']['name']);
        $type = $type[count($type) - 1];
        $image = 'uploads/toppers_talk/'.$config['file_name'].'.'.$type;
      }
     }else{
        echo json_encode(['status'=>402, 'message'=>'Please upload thumnail image.']);
        exit();
    }


     if(empty($video_link)){
      echo json_encode(['status'=>402, 'message'=>'Please enter a video link']);
      exit();
     }

     $data = array(
      'title'       => $title,
      'image'       => $image,
      'video_link'  => $video_link,
      'slug'        => $slug
     );

     $store = $this->setting_model->store_toppers($data);

    if($store){
      echo json_encode(['status'=>200, 'message'=>'Topper Talk added successfully!']);
     }else{
         echo json_encode(['status'=>302, 'message'=>'Something wrong happened']);   
     }
  }

  public function edit_toppers() {
    $this->not_admin_logged_in();
    $id = base64_decode($this->uri->segment(2));
    $data['page_title'] = "Edit Topper Talk";
    $data['topper'] = $this->setting_model->get_topper(array('id' => $id,'status' => 1));
    $this->admin_template('common/edit-toppers',$data);
  }

  public function update_toppers(){
    $id = $this->input->post('id');
    $title = $this->input->post('title');
    $video_link = $this->input->post('video_link');
    $slug = str_replace(' ','-',strtolower($title));
    $topper = $this->setting_model->get_topper(array('id' => $id,'status' => 1));
    if(empty($title)){
      echo json_encode(['status'=>402, 'message'=>'Please enter a title']);
      exit();
     }

     $this->load->library('upload');
    if($_FILES['image']['name'] != '')
		{
    $config = array(
      'upload_path' 	=> 'uploads/toppers_talk',
      'file_name' 	=> str_replace(' ','',$name).uniqid(),
      'allowed_types' => 'jpg|jpeg|png|gif|webp',
      'max_size' 		=> '10000000',
    );
        $this->upload->initialize($config);
    if ( ! $this->upload->do_upload('image'))
      {
          $error = $this->upload->display_errors();
          echo json_encode(['status'=>403, 'message'=>$error]);
          exit();
      }
      else
      {
        $type = explode('.',$_FILES['image']['name']);
        $type = $type[count($type) - 1];
        $image = 'uploads/toppers_talk/'.$config['file_name'].'.'.$type;
      }
     }else{
       $image = $topper->image;
    }


     if(empty($video_link)){
      echo json_encode(['status'=>402, 'message'=>'Please enter a video link']);
      exit();
     }

     $data = array(
      'title'       => $title,
      'image'       => $image,
      'video_link'  => $video_link
     );

     $update = $this->setting_model->update_toppers($data,$id);

    if($update){
      echo json_encode(['status'=>200, 'message'=>'Topper Talk updated successfully!']);
     }else{
         echo json_encode(['status'=>302, 'message'=>'Something wrong happened']);   
     }
  }


  public function delete_toppers(){
    $id = $this->input->post('id');

    $delete = $this->setting_model->delete_toppers($id);

    if($delete){
      echo json_encode(['status'=>200, 'message'=>'Topper Talk deleted successfully!']);
     }else{
         echo json_encode(['status'=>302, 'message'=>'Something wrong happened']);   
     }
  }


  public function mock_interview() {
    $this->not_admin_logged_in();
    $data['page_title'] = "Mock Interview";
    $this->admin_template('common/mock-interview',$data);
  }

  public function building_futures() {
    $this->not_admin_logged_in();
    $data['page_title'] = "Building Futures";
    $this->admin_template('common/building-futures',$data);
  }

  public function ajaxMockInterview(){
    $this->not_admin_logged_in();
		$condition = array('status'=>1);
		$interviews = $this->setting_model->make_datatables_mock_interview($condition); // this will call modal function for fetching data
		$data = array();
		foreach($interviews as $key=>$interview) // Loop over the data fetched and store them in array
		{
    $button = '';
			$sub_array = array();
      $button .= '<a href="'.base_url('edit-mock-interview/'.base64_encode($interview['id'])).'"  data-bs-toggle="tooltip" data-bs-placement="bottom" title="Edit Mock Interview" class="btn btn-sm bg-primary-light"><i class="fe fe-edit"></i> </a>';
      $button .= '<a href="javascript:void(0)" onclick="delete_mock_interview('.$interview['id'].')" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Delete Mock Interview" class="btn btn-sm bg-danger-light"><i class="fe fe-trash"></i> </a>';
			$image = '<img src="'.base_url($interview['image']).'" style="width:100px;height:100px"> ';
			$sub_array[] = $key+1;
      $sub_array[] = $image;
			$sub_array[] = $interview['title'];
			$sub_array[] = $interview['video_link'];
			$sub_array[] = date('d-F-Y', strtotime($interview['created_at']));
			$sub_array[] = $button;
		  $data[] = $sub_array;
		}
	
		$output = array(
			"draw"                    =>     intval($_POST["draw"]),
			"recordsTotal"            =>     $this->setting_model->get_all_data_mock_interview($condition),
			"recordsFiltered"         =>     $this->setting_model->get_filtered_data_mock_interview($condition),
			"data"                    =>     $data
		);
		
		echo json_encode($output);
  }

  public function create_mock_interview() {
    $this->not_admin_logged_in();
    $data['page_title'] = "Create Mock Interview";
    $this->admin_template('common/create-mock-interview',$data);
  }

  public function store_mock_interview(){
    $title = $this->input->post('title');
    $video_link = $this->input->post('video_link');
    $slug = str_replace(' ','-',strtolower($title));
    $check_interview = $this->setting_model->get_mock_interview(array('title' => $title));

    if(empty($title)){
      echo json_encode(['status'=>402, 'message'=>'Please enter a title']);
      exit();
     }

     if($check_interview){
      echo json_encode(['status'=>402, 'message'=>'This title aready exit']);
      exit();
     }

     $this->load->library('upload');
    if($_FILES['image']['name'] != '')
		{
    $config = array(
      'upload_path' 	=> 'uploads/mock_interview',
      'file_name' 	=> str_replace(' ','',$name).uniqid(),
      'allowed_types' => 'jpg|jpeg|png|gif|webp',
      'max_size' 		=> '10000000',
    );
        $this->upload->initialize($config);
    if ( ! $this->upload->do_upload('image'))
      {
          $error = $this->upload->display_errors();
          echo json_encode(['status'=>403, 'message'=>$error]);
          exit();
      }
      else
      {
        $type = explode('.',$_FILES['image']['name']);
        $type = $type[count($type) - 1];
        $image = 'uploads/mock_interview/'.$config['file_name'].'.'.$type;
      }
     }else{
        echo json_encode(['status'=>402, 'message'=>'Please upload thumnail image.']);
        exit();
    }


     if(empty($video_link)){
      echo json_encode(['status'=>402, 'message'=>'Please enter a video link']);
      exit();
     }

     $data = array(
      'title'       => $title,
      'image'       => $image,
      'video_link'  => $video_link,
      'slug'        => $slug
     );

     $store = $this->setting_model->store_mock_interview($data);

    if($store){
      echo json_encode(['status'=>200, 'message'=>'Mock Interview  added successfully!']);
     }else{
         echo json_encode(['status'=>302, 'message'=>'Something wrong happened']);   
     }
  }

  public function edit_mock_interview() {
    $this->not_admin_logged_in();
    $id = base64_decode($this->uri->segment(2));
    $data['page_title'] = "Edit Mock Interview";
    $data['interview'] = $this->setting_model->get_mock_interview(array('id' => $id,'status' => 1));
    $this->admin_template('common/edit-mock-interview',$data);
  }

  public function update_mock_interview(){
    $id = $this->input->post('id');
    $title = $this->input->post('title');
    $video_link = $this->input->post('video_link');
    $slug = str_replace(' ','-',strtolower($title));
    $interview = $this->setting_model->get_mock_interview(array('id' => $id,'status' => 1));
    $check_interview = $this->setting_model->get_mock_interview(array('title' => $title, 'id <>' => $id));

    if(empty($title)){
      echo json_encode(['status'=>402, 'message'=>'Please enter a title']);
      exit();
     }
     if($check_interview){
      echo json_encode(['status'=>402, 'message'=>'This Title already exit']);
      exit();
     }

     $this->load->library('upload');
    if($_FILES['image']['name'] != '')
		{
    $config = array(
      'upload_path' 	=> 'uploads/mock_interview',
      'file_name' 	=> str_replace(' ','',$name).uniqid(),
      'allowed_types' => 'jpg|jpeg|png|gif|webp',
      'max_size' 		=> '10000000',
    );
        $this->upload->initialize($config);
    if ( ! $this->upload->do_upload('image'))
      {
          $error = $this->upload->display_errors();
          echo json_encode(['status'=>403, 'message'=>$error]);
          exit();
      }
      else
      {
        $type = explode('.',$_FILES['image']['name']);
        $type = $type[count($type) - 1];
        $image = 'uploads/mock_interview/'.$config['file_name'].'.'.$type;
      }
     }else{
       $image = $interview->image;
    }


     if(empty($video_link)){
      echo json_encode(['status'=>402, 'message'=>'Please enter a video link']);
      exit();
     }
    
     

     $data = array(
      'title'       => $title,
      'image'       => $image,
      'video_link'  => $video_link,
      'slug'        => $slug
     );

     $update = $this->setting_model->update_mock_interview($data,$id);

    if($update){
      echo json_encode(['status'=>200, 'message'=>'Mock Interview updated successfully!']);
     }else{
         echo json_encode(['status'=>302, 'message'=>'Something wrong happened']);   
     }
  }


  public function delete_mock_interview(){
    $id = $this->input->post('id');

    $delete = $this->setting_model->delete_mock_interview($id);

    if($delete){
      echo json_encode(['status'=>200, 'message'=>'Mock Interview  deleted successfully!']);
     }else{
         echo json_encode(['status'=>302, 'message'=>'Something wrong happened']);   
     }
  }

  public function student_remark() {
    $this->not_admin_logged_in();
    $data['page_title'] = "Student Remark";
    $this->admin_template('common/student-remark',$data);
  }

  public function ajaxStudentRemark(){
    $this->not_admin_logged_in();
		$condition = array('status'=>1);
		$remarks = $this->setting_model->make_datatables_student_remark($condition); // this will call modal function for fetching data
		$data = array();
		foreach($remarks as $key=>$remark) // Loop over the data fetched and store them in array
		{
    $button = '';
			$sub_array = array();
      $button .= '<a href="'.base_url('edit-student-remark/'.base64_encode($remark['id'])).'"  data-bs-toggle="tooltip" data-bs-placement="bottom" title="Edit Student Remark" class="btn btn-sm bg-primary-light"><i class="fe fe-edit"></i> </a>';
      $button .= '<a href="javascript:void(0)" onclick="delete_student_remark('.$remark['id'].')" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Delete Student Remark" class="btn btn-sm bg-danger-light"><i class="fe fe-trash"></i> </a>';
			$image = '<img src="'.base_url($remark['image']).'" style="width:100px;height:100px"> ';
			$sub_array[] = $key+1;
      $sub_array[] = $image;
			$sub_array[] = $remark['title'];
			$sub_array[] = date('d-F-Y', strtotime($remark['created_at']));
			$sub_array[] = $button;
		  $data[] = $sub_array;
		}
	
		$output = array(
			"draw"                    =>     intval($_POST["draw"]),
			"recordsTotal"            =>     $this->setting_model->get_all_data_student_remark($condition),
			"recordsFiltered"         =>     $this->setting_model->get_filtered_data_student_remark($condition),
			"data"                    =>     $data
		);
		
		echo json_encode($output);
  }

  public function create_student_remark() {
    $this->not_admin_logged_in();
    $data['page_title'] = "Create Student Remark";
    $this->admin_template('common/create-student-remark',$data);
  }

  public function store_student_remark(){
    $title = $this->input->post('title');
    $video_link = $this->input->post('video_link');
    if(empty($title)){
      echo json_encode(['status'=>402, 'message'=>'Please enter a title']);
      exit();
     }

     $this->load->library('upload');
    if($_FILES['image']['name'] != '')
		{
    $config = array(
      'upload_path' 	=> 'uploads/student_remark',
      'file_name' 	=> str_replace(' ','',$name).uniqid(),
      'allowed_types' => 'jpg|jpeg|png|gif|webp',
      'max_size' 		=> '10000000',
    );
        $this->upload->initialize($config);
    if ( ! $this->upload->do_upload('image'))
      {
          $error = $this->upload->display_errors();
          echo json_encode(['status'=>403, 'message'=>$error]);
          exit();
      }
      else
      {
        $type = explode('.',$_FILES['image']['name']);
        $type = $type[count($type) - 1];
        $image = 'uploads/student_remark/'.$config['file_name'].'.'.$type;
      }
     }else{
        echo json_encode(['status'=>402, 'message'=>'Please upload thumnail image.']);
        exit();
    }

     $data = array(
      'title'       => $title,
      'image'       => $image,
     );

     $store = $this->setting_model->store_student_remark($data);

    if($store){
      echo json_encode(['status'=>200, 'message'=>'Student Remarks  added successfully!']);
     }else{
         echo json_encode(['status'=>302, 'message'=>'Something wrong happened']);   
     }
  }

  public function edit_student_remark() {
    $this->not_admin_logged_in();
    $id = base64_decode($this->uri->segment(2));
    $data['page_title'] = "Edit Student Remark";
    $data['remark'] = $this->setting_model->get_student_remark(array('id' => $id,'status' => 1));
    $this->admin_template('common/edit-student-remark',$data);
  }

  public function update_student_remark(){
    $id = $this->input->post('id');
    $title = $this->input->post('title');
    $remark = $this->setting_model->get_student_remark(array('id' => $id,'status' => 1));
    if(empty($title)){
      echo json_encode(['status'=>402, 'message'=>'Please enter a title']);
      exit();
     }

     $this->load->library('upload');
    if($_FILES['image']['name'] != '')
		{
    $config = array(
      'upload_path' 	=> 'uploads/mock_interview',
      'file_name' 	=> str_replace(' ','',$name).uniqid(),
      'allowed_types' => 'jpg|jpeg|png|gif|webp',
      'max_size' 		=> '10000000',
    );
        $this->upload->initialize($config);
    if ( ! $this->upload->do_upload('image'))
      {
          $error = $this->upload->display_errors();
          echo json_encode(['status'=>403, 'message'=>$error]);
          exit();
      }
      else
      {
        $type = explode('.',$_FILES['image']['name']);
        $type = $type[count($type) - 1];
        $image = 'uploads/mock_interview/'.$config['file_name'].'.'.$type;
      }
     }else{
       $image = $remark->image;
    }

     $data = array(
      'title'       => $title,
      'image'       => $image,
     );

     $update = $this->setting_model->update_student_remark($data,$id);

    if($update){
      echo json_encode(['status'=>200, 'message'=>'Student Remark updated successfully!']);
     }else{
         echo json_encode(['status'=>302, 'message'=>'Something wrong happened']);   
     }
  }


  public function delete_student_remark(){
    $id = $this->input->post('id');

    $delete = $this->setting_model->delete_student_remark($id);

    if($delete){
      echo json_encode(['status'=>200, 'message'=>'Student Remark  deleted successfully!']);
     }else{
         echo json_encode(['status'=>302, 'message'=>'Something wrong happened']);   
     }
  }

  public function ajaxBuildingFutures(){
    $this->not_admin_logged_in();
		$condition = array('status'=>1);
		$building_futures = $this->setting_model->make_datatables_building_futures($condition); // this will call modal function for fetching data
		$data = array();
		foreach($building_futures as $key=>$building_future) // Loop over the data fetched and store them in array
		{
    $button = '';
			$sub_array = array();
      $button .= '<a href="'.base_url('edit-building-futures/'.base64_encode($building_future['id'])).'"  data-bs-toggle="tooltip" data-bs-placement="bottom" title="Edit Building Futures" class="btn btn-sm bg-primary-light"><i class="fe fe-edit"></i> </a>';
      $button .= '<a href="javascript:void(0)" onclick="delete_building_futures('.$building_future['id'].')" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Delete Building Futures" class="btn btn-sm bg-danger-light"><i class="fe fe-trash"></i> </a>';
			$image = '<img src="'.base_url($building_future['image']).'" style="width:100px;height:100px"> ';
			$sub_array[] = $key+1;
      $sub_array[] = $image;
			$sub_array[] = $building_future['title'];
      $sub_array[] = $building_future['button_name'];
      $sub_array[] = $building_future['description'];
			$sub_array[] = $building_future['url'];
			$sub_array[] = date('d-F-Y', strtotime($building_future['created_at']));
			$sub_array[] = $button;
		  $data[] = $sub_array;
		}
	
		$output = array(
			"draw"                    =>     intval($_POST["draw"]),
			"recordsTotal"            =>     $this->setting_model->get_all_data_building_futures($condition),
			"recordsFiltered"         =>     $this->setting_model->get_filtered_data_building_futures($condition),
			"data"                    =>     $data
		);
		
		echo json_encode($output);
  }

  public function create_building_futures() {
    $this->not_admin_logged_in();
    $data['page_title'] = "Create Building Futures";
    $this->admin_template('common/create_building_futures',$data);
  }

  public function edit_building_futures() {
    $this->not_admin_logged_in();
    $id = base64_decode($this->uri->segment(2));
    $data['page_title'] = "Edit Building Futures";
    $condition = array('id' => $id);
    $data['building_future'] = $this->setting_model->get_building_future($condition);
    $this->admin_template('common/edit_building_futures',$data);
  }

  public function store_building_futures(){
    $title = $this->input->post('title');
    $description = $this->input->post('description');
    $button_name = $this->input->post('button_name');
    $url = $this->input->post('url');
    $slug = str_replace(' ','-',strtolower($title));
    $buildinFuturs = $this->setting_model->get_building_future(array('title' => $title));

    if(empty($title)){
      echo json_encode(['status'=>402, 'message'=>'Please enter a title']);
      exit();
     }
     if($buildinFuturs){
      echo json_encode(['status'=>402, 'message'=>'This title already exit']);
      exit();
     }
     if(empty($description)){
      echo json_encode(['status'=>402, 'message'=>'Please enter description']);
      exit();
     }
     if(empty($button_name)){
      echo json_encode(['status'=>402, 'message'=>'Please enter button name']);
      exit();
     }
   

     $this->load->library('upload');
    if($_FILES['image']['name'] != '')
		{
    $config = array(
      'upload_path' 	=> 'uploads/building_futures',
      'file_name' 	=> str_replace(' ','',$name).uniqid(),
      'allowed_types' => 'jpg|jpeg|png|gif',
      'max_size' 		=> '10000000',
    );
        $this->upload->initialize($config);
    if ( ! $this->upload->do_upload('image'))
      {
          $error = $this->upload->display_errors();
          echo json_encode(['status'=>403, 'message'=>$error]);
          exit();
      }
      else
      {
        $type = explode('.',$_FILES['image']['name']);
        $type = $type[count($type) - 1];
        $image = 'uploads/building_futures/'.$config['file_name'].'.'.$type;
      }
     }else{
        echo json_encode(['status'=>402, 'message'=>'Please upload image.']);
        exit();
    }

    $data = array(
      'title' => $title,
      'description' => $description,
      'button_name' => $button_name,
      'image' => $image,
      'url' => $url,
      'slug' => $slug,
    );
   
    $store = $this->setting_model->store_building_futures($data);
    if($store){
      echo json_encode(['status'=>200, 'message' => 'Building futures successfully submitted!']);
    }else{
      echo json_encode(['status'=>302, 'message' => 'Somthing went wrong!']);
    }
}

public function update_building_futures(){
  $futureID = $this->input->post('id');
  $title = $this->input->post('title');
  $description = $this->input->post('description');
  $button_name = $this->input->post('button_name');
  $url = $this->input->post('url');
  
  $slug = str_replace(' ','-',strtolower($title));
  $buildinFutur = $this->setting_model->get_building_future(array('id' => $futureID));
  $check_buildinFuturs = $this->setting_model->get_building_future(array('title' =>$title, 'id <>'=> $futureID));

  if(empty($title)){
    echo json_encode(['status'=>402, 'message'=>'Please enter a title']);
    exit();
   }
   if($check_buildinFuturs){
    echo json_encode(['status'=>402, 'message'=>'This title already exit']);
    exit();
   }

   if(empty($description)){
    echo json_encode(['status'=>402, 'message'=>'Please enter description']);
    exit();
   }
   if(empty($button_name)){
    echo json_encode(['status'=>402, 'message'=>'Please enter button name']);
    exit();
   }


   $this->load->library('upload');
  if($_FILES['image']['name'] != '')
  {
  $config = array(
    'upload_path' 	=> 'uploads/building_futures',
    'file_name' 	=> str_replace(' ','',$name).uniqid(),
    'allowed_types' => 'jpg|jpeg|png|gif',
    'max_size' 		=> '10000000',
  );
      $this->upload->initialize($config);
  if ( ! $this->upload->do_upload('image'))
    {
        $error = $this->upload->display_errors();
        echo json_encode(['status'=>403, 'message'=>$error]);
        exit();
    }
    else
    {
      $type = explode('.',$_FILES['image']['name']);
      $type = $type[count($type) - 1];
      $image = 'uploads/building_futures/'.$config['file_name'].'.'.$type;
    }
   }else{
    if(!empty($buildinFutur->image)){
      $image = $buildinFutur->image ;
    }else{
      echo json_encode(['status'=>402, 'message'=>'Please upload image.']);
      exit();
    }
      
  }

  $data = array(
    'title' => $title,
    'description' => $description,
    'button_name' => $button_name,
    'image' => $image,
    'url' => $url,
    'slug' => $slug,
  );
 
  $update = $this->setting_model->update_building_futures($data,$futureID);
  if($update){
    echo json_encode(['status'=>200, 'message' => 'Building futures successfully Updated!']);
  }else{
    echo json_encode(['status'=>302, 'message' => 'Somthing went wrong!']);
  }
}

function delete_building_futures(){
    $id = $this->input->post('id');
    $delete = $this->setting_model->delete_building_futures($id);

    if($delete){
      echo json_encode(['status'=>200, 'message'=>'Building futures deleted successfully!']);
     }else{
       echo json_encode(['status'=>302, 'message'=>'Something wrong happened']);   
     }
}

public function vocabulary() {
  $this->not_admin_logged_in();
  $data['page_title'] = "Vocabulary";
  $this->admin_template('common/vocabulary',$data);
}


public function ajaxvocabulary(){
  $this->not_admin_logged_in();
  $condition = array('status'=>1);
  $vocabularies = $this->setting_model->make_datatables_vocabulary($condition); // this will call modal function for fetching data
  $data = array();
  foreach($vocabularies as $key=>$vocabulary) // Loop over the data fetched and store them in array
  {
  $button = '';
    $sub_array = array();
    $button .= '<a href="'.base_url('edit-vocabulary/'.base64_encode($vocabulary['id'])).'"  data-bs-toggle="tooltip" data-bs-placement="bottom" title="Edit Building Futures" class="btn btn-sm bg-primary-light"><i class="fe fe-edit"></i> </a>';
    $button .= '<a href="javascript:void(0)" onclick="delete_vocabulary('.$vocabulary['id'].')" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Delete Building Futures" class="btn btn-sm bg-danger-light"><i class="fe fe-trash"></i> </a>';
    $image = '<img src="'.base_url($vocabulary['image']).'" style="width:100px;height:100px"> ';
    $sub_array[] = $key+1;
    $sub_array[] = $image;
    $sub_array[] = $vocabulary['title'];
    $sub_array[] = $vocabulary['description'];
    $sub_array[] = date('d-F-Y', strtotime($vocabulary['created_at']));
    $sub_array[] = $button;
    $data[] = $sub_array;
  }

  $output = array(
    "draw"                    =>     intval($_POST["draw"]),
    "recordsTotal"            =>     $this->setting_model->get_all_data_vocabulary($condition),
    "recordsFiltered"         =>     $this->setting_model->get_filtered_vocabulary($condition),
    "data"                    =>     $data
  );
  
  echo json_encode($output);
}

public function create_vocabulary(){
  $this->not_admin_logged_in();
    $data['page_title'] = "Create Vocabulary";
    $this->admin_template('common/create_vocabulary',$data);
}

public function edit_vocabulary(){
  $this->not_admin_logged_in();
    $id = base64_decode($this->uri->segment(2));
    $data['page_title'] = "Edit Vocabulary";
    $condition = array('id' => $id);
    $data['vocabulary'] = $this->setting_model->get_vocabulary($condition);
    $this->admin_template('common/edit_vocabulary',$data);
}

public function store_vocabulary(){
  $title = $this->input->post('title');
  $description = $this->input->post('description');
  $slug = str_replace(' ','-',strtolower($title));
  $vocabulary = $this->setting_model->get_vocabulary(array('title' => $title));

  if(empty($title)){
    echo json_encode(['status'=>402, 'message'=>'Please enter a title']);
    exit();
   }

   if($vocabulary){
    echo json_encode(['status'=>402, 'message'=>'This title already exit']);
    exit();
   }
 
   if(empty($description)){
    echo json_encode(['status'=>402, 'message'=>'Please enter description']);
    exit();
   }


   $this->load->library('upload');
  if($_FILES['image']['name'] != '')
  {
  $config = array(
    'upload_path' 	=> 'uploads/vocabulary',
    'file_name' 	=> str_replace(' ','',$name).uniqid(),
    'allowed_types' => 'jpg|jpeg|png|gif',
    'max_size' 		=> '10000000',
  );
      $this->upload->initialize($config);
  if ( ! $this->upload->do_upload('image'))
    {
        $error = $this->upload->display_errors();
        echo json_encode(['status'=>403, 'message'=>$error]);
        exit();
    }
    else
    {
      $type = explode('.',$_FILES['image']['name']);
      $type = $type[count($type) - 1];
      $image = 'uploads/vocabulary/'.$config['file_name'].'.'.$type;
    }
   }else{
      echo json_encode(['status'=>402, 'message'=>'Please upload image.']);
      exit();
  }

  $data = array(
    'title' => $title,
    'description' => $description,
    'image' => $image,
    'slug' => $slug,
  );
 
  $store = $this->setting_model->store_vocabulary($data);
  if($store){
    echo json_encode(['status'=>200, 'message' => 'Vocabulary successfully submitted!']);
  }else{
    echo json_encode(['status'=>302, 'message' => 'Somthing went wrong!']);
  }
}

function update_vocabulary(){
  $vocabularyID = $this->input->post('id');
  $title = $this->input->post('title');
  $description = $this->input->post('description');
  $slug = str_replace(' ','-',strtolower($title));

  $vocabulary = $this->setting_model->get_vocabulary(array('id' => $vocabularyID));
  //print($vocabulary); die;
  $check_vocabulary = $this->setting_model->get_vocabulary(array('title' => $title, 'id <>' => $vocabularyID));

  // print_r($buildinFuturs); die;
  if(empty($title)){
    echo json_encode(['status'=>402, 'message'=>'Please enter a title']);
    exit();
   }
   if($check_vocabulary){
    echo json_encode(['status'=>402, 'message'=>'This title already exit']);
    exit();
   }

   if(empty($description)){
    echo json_encode(['status'=>402, 'message'=>'Please enter description']);
    exit();
   }
 
   $this->load->library('upload');
  if($_FILES['image']['name'] != '')
  {
  $config = array(
    'upload_path' 	=> 'uploads/vocabulary',
    'file_name' 	=> str_replace(' ','',$title).uniqid(),
    'allowed_types' => 'jpg|jpeg|png|gif',
    'max_size' 		=> '10000000',
  );
      $this->upload->initialize($config);
  if ( ! $this->upload->do_upload('image'))
    {
        $error = $this->upload->display_errors();
        echo json_encode(['status'=>403, 'message'=>$error]);
        exit();
    }
    else
    {
      $type = explode('.',$_FILES['image']['name']);
      $type = $type[count($type) - 1];
      $image = 'uploads/vocabulary/'.$config['file_name'].'.'.$type;
    }
   }else{
    if(!empty($vocabulary->image)){
      $image = $vocabulary->image ;
    }else{
      echo json_encode(['status'=>402, 'message'=>'Please upload image.']);
      exit();
    }
      
  }

  $data = array(
    'title' => $title,
    'description' => $description,
    'image' => $image,
    'slug' => $slug,
  );
 
  $update = $this->setting_model->update_vocabulary($data,$vocabularyID);
  if($update){
    echo json_encode(['status'=>200, 'message' => 'Vocabulary successfully Updated!']);
  }else{
    echo json_encode(['status'=>302, 'message' => 'Somthing went wrong!']);
  }
}

function delete_vocabulary(){
  $id = $this->input->post('id');
  $delete = $this->setting_model->delete_vocabulary($id);

  if($delete){
    echo json_encode(['status'=>200, 'message'=>'Vocabulary deleted successfully!']);
   }else{
     echo json_encode(['status'=>302, 'message'=>'Something wrong happened']);   
   }
}


public function upcoming_exam() {
  $this->not_admin_logged_in();
  $data['page_title'] = "Upcoming Exam";
  $this->admin_template('common/upcoming_exam',$data);
}


public function ajaxUpcomingExam(){
  $this->not_admin_logged_in();
  $condition = array('status'=>1);
  $upcomingExams = $this->setting_model->make_datatables_upcomingExam($condition); // this will call modal function for fetching data
  $data = array();
  foreach($upcomingExams as $key=>$upcomingExam) // Loop over the data fetched and store them in array
  {
  $button = '';
    $sub_array = array();
    $button .= '<a href="'.base_url('edit-upcoming-exam/'.base64_encode($upcomingExam['id'])).'"  data-bs-toggle="tooltip" data-bs-placement="bottom" title="Edit Building Futures" class="btn btn-sm bg-primary-light"><i class="fe fe-edit"></i> </a>';
    $button .= '<a href="javascript:void(0)" onclick="delete_upcomingExam('.$upcomingExam['id'].')" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Delete Building Futures" class="btn btn-sm bg-danger-light"><i class="fe fe-trash"></i> </a>';
    $image = '<img src="'.base_url($upcomingExam['image']).'" style="width:100px;height:100px"> ';
    $sub_array[] = $key+1;
    $sub_array[] = $upcomingExam['title'];
    $sub_array[] = $upcomingExam['description'];
    $sub_array[] = $upcomingExam['link'];
    $sub_array[] = date('d-M-Y', strtotime($upcomingExam['created_at']));
    $sub_array[] = $button;
    $data[] = $sub_array;
  }

  $output = array(
    "draw"                    =>     intval($_POST["draw"]),
    "recordsTotal"            =>     $this->setting_model->get_all_data_upcomingExam($condition),
    "recordsFiltered"         =>     $this->setting_model->get_filtered_data_upcomingExam($condition),
    "data"                    =>     $data
  );
  
  echo json_encode($output);
}

public function create_upcoming_exam(){
  $this->not_admin_logged_in();
    $data['page_title'] = "Create Upcoming exam";
    $this->admin_template('common/create_upcoming_exam',$data);
}

public function edit_upcoming_exam(){
  $this->not_admin_logged_in();
    $id = base64_decode($this->uri->segment(2));
    $data['page_title'] = "Edit Upcoming Exam";
    $condition = array('id' => $id);
    $data['upcomingExams'] = $this->setting_model->get_upcoming_exam($condition);
    $this->admin_template('common/edit_upcoming_exam',$data);
}

public function store_upcomingExam(){
  $title = $this->input->post('title');
  $description = $this->input->post('description');
  $link = $this->input->post('link');
  $slug = str_replace(' ','-',strtolower($title));
 $upcomingExams = $this->setting_model->get_upcoming_exam(array('title' => $title));

  if(empty($title)){
    echo json_encode(['status'=>402, 'message'=>'Please enter a title']);
    exit();
   }

   if($upcomingExams){
    echo json_encode(['status'=>402, 'message'=>'This title already exit']);
    exit();
   }
 
   if(empty($description)){
    echo json_encode(['status'=>402, 'message'=>'Please enter description']);
    exit();
   }
   if(empty($link)){
    echo json_encode(['status'=>402, 'message'=>'Please enter link']);
    exit();
   }

  $this->load->library('upload');
  if($_FILES['image']['name'] != '')
  {
  $config = array(
    'upload_path' 	=> 'uploads/upcomingExam',
    'file_name' 	=> str_replace(' ','',$title).uniqid(),
    'allowed_types' => 'jpg|jpeg|png|gif',
    'max_size' 		=> '10000000',
  );
      $this->upload->initialize($config);
  if ( ! $this->upload->do_upload('image'))
    {
        $error = $this->upload->display_errors();
        echo json_encode(['status'=>403, 'message'=>$error]);
        exit();
    }
    else
    {
      $type = explode('.',$_FILES['image']['name']);
      $type = $type[count($type) - 1];
      $image = 'uploads/upcomingExam/'.$config['file_name'].'.'.$type;
    }
   }else{
      echo json_encode(['status'=>402, 'message'=>'Please upload image.']);
      exit();
    }
      
   
  $data = array(
    'title' => $title,
    'description' => $description,
    'link' => $link,
    'slug' => $slug,
    'image' => $image
  );
 
  $store = $this->setting_model->store_upcomingExam($data);
  if($store){
    echo json_encode(['status'=>200, 'message' => 'Upcoming exam successfully submitted!']);
  }else{
    echo json_encode(['status'=>302, 'message' => 'Somthing went wrong!']);
  }
}

function update_upcomingExam(){
  $id = $this->input->post('id');
  $title = $this->input->post('title');
  $description = $this->input->post('description');
  $link = $this->input->post('link');
  $slug = str_replace(' ','-',strtolower($title));
  $upcomingExam = $this->setting_model->get_upcoming_exam(array('id' => $id));
  $check_upcomingExam = $this->setting_model->get_upcoming_exam(array('title' =>$title, 'id <>'=> $id));

  // print_r($buildinFuturs); die;
  if(empty($title)){
    echo json_encode(['status'=>402, 'message'=>'Please enter a title']);
    exit();
   }

   if($check_upcomingExam){
    echo json_encode(['status'=>402, 'message'=>'This title already exit']);
    exit();
   }

   if(empty($description)){
    echo json_encode(['status'=>402, 'message'=>'Please enter description']);
    exit();
   }

   if(empty($link)){
    echo json_encode(['status'=>402, 'message'=>'Please enter link']);
    exit();
   }

   $this->load->library('upload');
   if($_FILES['image']['name'] != '')
   {
   $config = array(
     'upload_path' 	=> 'uploads/upcomingExam',
     'file_name' 	=> str_replace(' ','',$title).uniqid(),
     'allowed_types' => 'jpg|jpeg|png|gif',
     'max_size' 		=> '10000000',
   );
       $this->upload->initialize($config);
   if ( ! $this->upload->do_upload('image'))
     {
         $error = $this->upload->display_errors();
         echo json_encode(['status'=>403, 'message'=>$error]);
         exit();
     }
     else
     {
       $type = explode('.',$_FILES['image']['name']);
       $type = $type[count($type) - 1];
       $image = 'uploads/upcomingExam/'.$config['file_name'].'.'.$type;
     }
    }else{
     if(!empty($upcomingExam->image)){
       $image = $upcomingExam->image ;
     }else{
       echo json_encode(['status'=>402, 'message'=>'Please upload image.']);
       exit();
     }
       
   }

  $data = array(
    'title'       => $title,
    'description' => $description,
    'link'        => $link,
    'image'       => $image,
    'slug'        => $slug,
  );
 
  $update = $this->setting_model->update_upcomingExam($data,$id);
  if($update){
    echo json_encode(['status'=>200, 'message' => 'Upcoming Exam successfully Updated!']);
  }else{
    echo json_encode(['status'=>302, 'message' => 'Somthing went wrong!']);
  }
}

function delete_upcomingExam(){
  $id = $this->input->post('id');
  $delete = $this->setting_model->delete_upcomingExam($id);

  if($delete){
    echo json_encode(['status'=>200, 'message'=>'Upcoming Exam deleted successfully!']);
   }else{
     echo json_encode(['status'=>302, 'message'=>'Something wrong happened']);   
   }
}








}




?>