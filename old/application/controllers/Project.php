<?php 
class Project extends MY_Controller 
{
	public function __construct()
	{
		parent::__construct();
		$this->not_admin_logged_in();
		$this->load->model('project_model');
		$this->load->model('user_model');
	}

	public function index()
	{
		$menuID = 4;
		$permission = $this->permissions()->$menuID;
		if($permission[0]=='View'){
			$data['page_title'] = 'Project';
			$data['permission'] = $permission[1];
			$this->admin_template('project/index',$data);
		}else{
			redirect(base_url('dashboard'));
		}
	}

	public function ajaxProject(){
		$condition = array('project.status'=>1);
		$projects = $this->project_model->make_datatables($condition); // this will call modal function for fetching data
		$data = array();
		$menuID = 4;
		$permission = $this->permissions()->$menuID;
		//print_r($permission);
		foreach($projects as $key=>$project) // Loop over the data fetched and store them in array
		{
		
			$button = '';
			$sub_array = array();
			if($permission[0]=='View'){
				$button .= '<a href="'.base_url('view-project/'.base64_encode($project['id'])).'" data-bs-toggle="tooltip" data-bs-placement="bottom" title="View Project Detail" class="btn  btn-sm  text-warning"><i class="fa fa-eye"></i></a>';
			}
			if($permission[2]=='Edit'){
		  	$button .= '<a href="'.base_url('edit-project/'.base64_encode($project['id'])).'" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Edit Project Detail" class="btn  btn-sm  text-primary"><i class="fa fa-edit"></i> </a>';
			}
			if($permission[3]=='Delete'){
			 $button .= '<a href="javascript:void(0)" onclick="delete_project('.$project['id'].')" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Delete Project" class="btn  btn-sm  text-danger"><i class="fa fa-trash"></i> </a>';
			}
			$sub_array[] = $key+1;
			$sub_array[] = $project['name'];
			$sub_array[] = $project['userName'];
			$sub_array[] = $project['userPhone'];
			$sub_array[] = $project['userAddress'];
			$sub_array[] = date('d-m-Y', strtotime($project['created_at']));
			$sub_array[] = $button;
		  $data[] = $sub_array;
		}
	
		$output = array(
			"draw"                    =>     intval($_POST["draw"]),
			"recordsTotal"            =>     $this->project_model->get_all_data($condition),
			"recordsFiltered"         =>     $this->project_model->get_filtered_data($condition),
			"data"                    =>     $data
		);
		
		echo json_encode($output);
	}


	public function create()
	{
    $data['page_title'] = 'Create Project';
		$data['users'] = $this->user_model->get_users(array('users.status'=>1,'users.user_type'=>4));
	  $this->admin_template('project/create-project',$data);
	}

	public function store(){

		$name = $this->input->post('name');
		$clientID = $this->input->post('clientID');
		$start_chainage = $this->input->post('start_chainage');
		$end_chainage = $this->input->post('end_chainage');
		if(empty($name)){
			echo json_encode(['status'=>403, 'message'=>'Please enter project name']); 	
			exit();
		}
		$project = $this->project_model->get_project(array('name'=>$name));
		if($project){
			echo json_encode(['status'=>403, 'message'=>'This project already exists']); 	
			exit();
		}

		if(empty($clientID)){
			echo json_encode(['status'=>403, 'message'=>'Please select client']); 	
			exit();
		}


// 		if(empty($start_chainage)){
// 			echo json_encode(['status'=>403, 'message'=>'Please enter a start chainage']); 	
// 			exit();
// 		}

		if(empty($end_chainage)){
			echo json_encode(['status'=>403, 'message'=>'Please enter a end chainage']); 	
			exit();
		}

		$data = array(
			'name'=>$name,
			'clientID'=>$clientID,
			'start_chainage' => $start_chainage,
			'end_chainage'   => $end_chainage,
		);

		$projectID = $this->project_model->store_project($data);

		if($projectID){

			 $total_chainage =  count(array_filter($this->input->post('chainage')));
      for($i=0; $i<$total_chainage; $i++){
			  $chainage = $this->input->post('chainage')[$i];
				$chainageData = array(
					'projectID'   => $projectID,
					'chainage'    => $chainage,
					'description' => ''
				);

			 $storeChainage = $this->project_model->store_chainage($chainageData);
		}
			echo json_encode(['status'=>200, 'message'=>'Project created successfully']); 	
			exit();
		}else{
			echo json_encode(['status'=>403, 'message'=>'Something went wrong']); 	
			exit();
		}

	}


	public function edit()
	{
		$id = base64_decode($this->uri->segment(2));
		$menuID = 4;
		$permission = $this->permissions()->$menuID;
		if($permission[2]=='Edit'){
    $data['page_title'] = 'Edit Project';
		$data['project'] = $this->project_model->get_project(array('id'=>$id));
		$data['users'] = $this->user_model->get_users(array('users.status'=>1,'users.user_type'=>4));
		$data['chainages'] =$this->project_model->get_chainages(array('projectID'=>$id));
 	  $this->admin_template('project/edit-project',$data);
		}else{
			redirect(base_url('dashboard'));
		}
	}

	public function view()
	{
		$id = base64_decode($this->uri->segment(2));
		$menuID = 4;
		$permission = $this->permissions()->$menuID;
		if($permission[0]=='View'){
    $data['page_title'] = 'View Project';
		$data['project'] = $this->project_model->get_project(array('id'=>$id));
		$data['users'] = $this->user_model->get_users(array('users.status'=>1,'users.user_type'=>4));
		$data['chainages'] =$this->project_model->get_chainages(array('projectID'=>$id));
 	  $this->admin_template('project/view-project',$data);
	}else{
		redirect(base_url('dashboard'));
	}
	}


	// public function update(){
  //   $id = $this->input->post('projectID');
	// 	$name = $this->input->post('name');
	// 	$start_chainage = $this->input->post('start_chainage');
	// 	$end_chainage = $this->input->post('end_chainage');
	// 	if(empty($name)){
	// 		echo json_encode(['status'=>403, 'message'=>'Please enter project name']); 	
	// 		exit();
	// 	}
	// 	$project = $this->project_model->get_project(array('name'=>$name,'id<>'=>$id));
	// 	if($project){
	// 		echo json_encode(['status'=>403, 'message'=>'This project already exists']); 	
	// 		exit();
	// 	}
	// 	if(empty($start_chainage)){
	// 		echo json_encode(['status'=>403, 'message'=>'Please enter a start chainage']); 	
	// 		exit();
	// 	}

	// 	if(empty($end_chainage)){
	// 		echo json_encode(['status'=>403, 'message'=>'Please enter a end chainage']); 	
	// 		exit();
	// 	}

	// 	$data = array(
	// 		'name'=>$name,
	// 		'start_chainage' => $start_chainage,
	// 		'end_chainage'   => $end_chainage,
	// 	);

	// 	$update = $this->project_model->update_project($data,$id);

	// 	if($update){

	// 		$old_chainage = $this->input->post('chainage_old');
	// 		$description_old = $this->input->post('description_old');
      
	// 		foreach($old_chainage as $key=>$chain){

	// 			$old_chainage_data = array(
	// 				'chainage' => $chain,
	// 				'description' => $this->input->post('description_old')[$key]
	// 			);

	// 			$this->project_model->update_chainage($old_chainage_data,$key);
	// 		}

	// 		 $total_chainage =  count(array_filter($this->input->post('chainage')));

	// 		if($total_chainage > 0){
  //     for($i=0; $i<$total_chainage; $i++){
	// 		  $chainage = $this->input->post('chainage')[$i];
	// 			$description = $this->input->post('description')[$i];
	// 			$chainageData = array(
	// 				'projectID'   => $id,
	// 				'chainage'    => $chainage,
	// 				'description' => $description
	// 			);

	// 		 $storeChainage = $this->project_model->store_chainage($chainageData);
	// 	  }
	//   }
	// 		echo json_encode(['status'=>200, 'message'=>'Project created successfully']); 	
	// 		exit();
	// 	}else{
	// 		echo json_encode(['status'=>403, 'message'=>'Something went wrong']); 	
	// 		exit();
	// 	}

	// }

	public function update(){
    $id = $this->input->post('projectID');
		$name = $this->input->post('name');
		$clientID = $this->input->post('clientID');
		// $contact_no = $this->input->post('contact_no');
		// $contact_address = $this->input->post('contact_address');
		$start_chainage = $this->input->post('start_chainage');
		$end_chainage = $this->input->post('end_chainage');
		if(empty($name)){
			echo json_encode(['status'=>403, 'message'=>'Please enter project name']); 	
			exit();
		}
		$project = $this->project_model->get_project(array('name'=>$name,'id<>'=>$id));
		if($project){
			echo json_encode(['status'=>403, 'message'=>'This project already exists']); 	
			exit();
		}

		if(empty($clientID)){
			echo json_encode(['status'=>403, 'message'=>'Please select client']); 	
			exit();
		}

		// if(empty($contact_no)){
		// 	echo json_encode(['status'=>403, 'message'=>'Please enter contact no.']); 	
		// 	exit();
		// }

		// if(empty($contact_address)){
		// 	echo json_encode(['status'=>403, 'message'=>'Please enter location/address']); 	
		// 	exit();
		// }

// 		if(empty($start_chainage)){
// 			echo json_encode(['status'=>403, 'message'=>'Please enter a start chainage']); 	
// 			exit();
// 		}

		if(empty($end_chainage)){
			echo json_encode(['status'=>403, 'message'=>'Please enter a end chainage']); 	
			exit();
		}

		$data = array(
			'name'=>$name,
			'clientID'=>$clientID,
			// 'contact_no'=>$contact_no,
			// 'contact_address'=>$contact_address,
			'start_chainage' => $start_chainage,
			'end_chainage'   => $end_chainage,
		);

		$update = $this->project_model->update_project($data,$id);

		if($update){

			$delete = $this->project_model->delete_chainage($id);

			$total_chainage =  count(array_filter($this->input->post('chainage')));
      for($i=0; $i<$total_chainage; $i++){
			  $chainage = $this->input->post('chainage')[$i];
				$chainageData = array(
					'projectID'   => $id,
					'chainage'    => $chainage,
					'description' => ''
				);

			 $storeChainage = $this->project_model->store_chainage($chainageData);
		}
	  
			echo json_encode(['status'=>200, 'message'=>'Project created successfully']); 	
			exit();
		}else{
			echo json_encode(['status'=>403, 'message'=>'Something went wrong']); 	
			exit();
		}

	}

	public function delete(){
		 $id = $this->input->post('id');
		 $project_data = array(
			'status' => 2,
		 );

		 $delete = $this->project_model->update_project($project_data,$id);
		 if($delete){
			$inspection_data = array(
				'status' => 2,
			 );
	
			 $update = $this->project_model->update_road_inspection_maintenance($inspection_data,array('projectID'=>$id));
			 echo json_encode(['status'=>200, 'message'=>'Project deleted successfully']); 	
			 exit();
		 }else{
			 echo json_encode(['status'=>403, 'message'=>'Something went wrong']); 	
			 exit();
		 }

	}


	// Work Performed
	public function observation(){
		$id = base64_decode($this->uri->segment(2));
		$menuID = 5;
		$permission = $this->permissions()->$menuID;
		if($permission[0]=='View'){
    $data['page_title'] = 'Defect';
		$data['permission'] = $permission[1];
	  $this->admin_template('work_perform/index',$data);
		}else{
			redirect(base_url('dashboard'));
		}
	}


	public function ajaxObservation(){
		$condition = array('status'=>1);
		$observations = $this->project_model->make_datatables_observation($condition); // this will call modal function for fetching data
		$data = array();
		$id = base64_decode($this->uri->segment(2));
		$menuID = 5;
		$permission = $this->permissions()->$menuID;
		
		foreach($observations as $key=>$observation) // Loop over the data fetched and store them in array
		{
			$button = '';
			$sub_array = array();
			if($permission[0]=='View'){
			 $button .= '<a href="javascript:void(0)" onclick="view_observation('.$observation['id'].')" data-bs-toggle="tooltip" data-bs-placement="bottom" title="View observation Detail" class="btn  btn-sm  text-warning"><i class="fa fa-eye"></i></a>';
			}if($permission[2]=='Edit'){
			 $button .= '<a href="javascript:void(0)" onclick="edit_observation('.$observation['id'].')" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Edit observation Detail" class="btn  btn-sm  text-primary"><i class="fa fa-edit"></i> </a>';
			}
			$sub_array[] = $key+1;
			$sub_array[] = $observation['name'];
			$sub_array[] = date('d-m-Y', strtotime($observation['created_at']));
			$sub_array[] = $button;
		  $data[] = $sub_array;
		}
	
		$output = array(
			"draw"                    =>     intval($_POST["draw"]),
			"recordsTotal"            =>     $this->project_model->get_all_data_observation($condition),
			"recordsFiltered"         =>     $this->project_model->get_filtered_data_observation($condition),
			"data"                    =>     $data
		);
		
		echo json_encode($output);
	}


	public function store_observation(){
		$name = $this->input->post('name');
		if(empty($name)){
			echo json_encode(['status'=>403, 'message'=>'Please enter Work performed name']); 	
			exit();
		}
		$observation = $this->project_model->get_observation(array('name'=>$name));
		if($observation){
			echo json_encode(['status'=>403, 'message'=>'This Work performed already exists']); 	
			exit();
		}

		$data = array(
			'name'=>$name,
		);

		$store = $this->project_model->store_observation($data);

		if($store){
			echo json_encode(['status'=>200, 'message'=>'Work performed created successfully']); 	
			exit();
		}else{
			echo json_encode(['status'=>403, 'message'=>'Something went wrong']); 	
			exit();
		}
	}

	public function observation_edit_form(){
		$id = $this->input->post('id');
		$observation = $this->project_model->get_observation(array('id'=>$id));
		?>
		<div class="form-group">
			<label for="recipient-name" class="col-form-label">Defect Name:</label>
			<input type="text" class="form-control" name="name" id="name" value="<?=$observation->name?>"
				placehoder="Defect Name">
		</div>

		<input type="hidden" name="id" id="id" value="<?=$id?>">
   <?php
	}
  

	public function update_observation(){
		$id = $this->input->post('id');
		$name = $this->input->post('name');
		if(empty($name)){
			echo json_encode(['status'=>403, 'message'=>'Please enter Work performed name']); 	
			exit();
		}
		$observation = $this->project_model->get_observation(array('name'=>$name,'id<>'=>$id));
		if($observation){
			echo json_encode(['status'=>403, 'message'=>'This Work performed already exists']); 	
			exit();
		}

		$data = array(
			'name'=>$name,
		);

		$update = $this->project_model->update_observation($data,$id);

		if($update){
			echo json_encode(['status'=>200, 'message'=>'Work performed updated successfully']); 	
			exit();
		}else{
			echo json_encode(['status'=>403, 'message'=>'Something went wrong']); 	
			exit();
		}
	}


	public function observation_view_form(){
		$id = $this->input->post('id');
		$observation = $this->project_model->get_observation(array('id'=>$id));
		?>
		<div class="form-group">
			<label for="recipient-name" class="col-form-label">Work performed Name:</label>
			<input type="text" class="form-control" name="name" id="name" readonly value="<?=$observation->name?>"
				placehoder="Work performed Name">
		</div>
   <?php
	}

	// Road Inspection Maintenance
	public function road_inspection_maintenance(){
		$menuID = 6;
		$permission = $this->permissions()->$menuID;
		if($permission[0]=='View'){
			$data['page_title'] = 'Road Inspection Maintenance';
			$data['permission'] = $permission;
			$data['projects']=$this->project_model->get_projects(array('status'=>1));
			$this->admin_template('road_inspection_maintenance/index',$data);
		}else{
			redirect(base_url('dashboard'));
		}
	}


	public function ajaxRoadInspectionMaintenance(){
		$condition = array('road_inspetion_maintenance.projectID'=>$this->session->userdata('project'),'road_inspetion_maintenance.status'=>1);
		$inspections = $this->project_model->make_datatables_road_inspection_maintenance($condition); // this will call modal function for fetching data
		$data = array();
		$menuID = 6;
		$permission = $this->permissions()->$menuID;
		
		foreach($inspections as $key=>$inspection) // Loop over the data fetched and store them in array
		{
			$button = '';
			$sub_array = array();
			if($permission[0]=='View'){
			$button .= '<a href="'.base_url('view-road-inspection-maintenance/'.base64_encode($inspection['id'])).'" data-bs-toggle="tooltip" data-bs-placement="bottom" title="View Project Detail" class="btn  btn-sm  text-warning"><i class="fa fa-eye"></i></a>';
			}
// 			if($permission[2]=='Edit'){
// 			$button .= '<a href="'.base_url('edit-road-inspection-maintenance/'.base64_encode($inspection['id'])).'" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Edit Project Detail" class="btn  btn-sm  text-primary"><i class="fa fa-edit"></i> </a>';
// 			}
			if($permission[3]=='Delete'){
			$button .= '<a href="javascript:void(0)" onclick="delete_road_inspection_maintenance('.$inspection['id'].')" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Delete Project" class="btn  btn-sm  text-danger"><i class="fa fa-trash"></i> </a>';
	  }
 
		 $location = explode("/", $inspection['location']);

			$image = '<a href="'.base_url($inspection['image']).'" target="_blank"><img src="'.base_url($inspection['image']).'"  width="100" height="100"></a>';
			$sub_array[] = $key+1;
			// $sub_array[] = $inspection['projectName'];
			$sub_array[] = $inspection['chainage'];
			$sub_array[] = $location[0];
			$sub_array[] = $location[1];
			$sub_array[] = date('d-m-Y', strtotime($inspection['inspection_date']));
			$sub_array[] = date('H:i A', strtotime($inspection['inspection_time']));
			$sub_array[] = $inspection['workPerformName'];
			$sub_array[] = $inspection['note']; 
			$sub_array[] = $inspection['inspector_name'];
			$sub_array[] = $image;
			$sub_array[] = $button ;
		  $data[] = $sub_array;
		}
	
		$output = array(
			"draw"                    =>     intval($_POST["draw"]),
			"recordsTotal"            =>     $this->project_model->get_all_data_road_inspection_maintenance($condition),
			"recordsFiltered"         =>     $this->project_model->get_filtered_data_road_inspection_maintenance($condition),
			"data"                    =>     $data
		);
		
		echo json_encode($output);
	}
	public function create_road_inspection_maintenance(){
		$menuID = 6;
		$permission = $this->permissions()->$menuID;
		if($permission[1]=='Add'){
    $data['page_title'] = 'Create Road Inspection Maintenance';
		$data['projects']=$this->project_model->get_projects(array('status'=>1));
		$data['observations']=$this->project_model->get_observations(array('status'=>1));
	  $this->admin_template('road_inspection_maintenance/create',$data);
		}else{
     redirect(base_url('dashboard'));
		}
	}

	public function store_road_inspection_maintenance(){
		$projectID = $this->input->post('projectID');
		$location = $this->input->post('location');
		$chainage = $this->input->post('chainage');
		$inspection_date = $this->input->post('inspection_date');
		$inspection_time = $this->input->post('inspection_time');
		$work_perform = $this->input->post('work_perform');
		$note = $this->input->post('note');
		$inspector_name = $this->input->post('inspector_name');

		if(empty($projectID)){
			echo json_encode(['status'=>403, 'message'=>'Please select project']); 	
			exit();
		}

		if(empty($location)){
			echo json_encode(['status'=>403, 'message'=>'Please enter location']); 	
			exit();
		}

		if(empty($chainage)){
			echo json_encode(['status'=>403, 'message'=>'Please select chainage']); 	
			exit();
		}

		if(empty($inspection_date)){
			echo json_encode(['status'=>403, 'message'=>'Please enter inspection date']); 	
			exit();
		}

		if(empty($inspection_time)){
			echo json_encode(['status'=>403, 'message'=>'Please enter inspection time']); 	
			exit();
		}

		if(empty($work_perform)){
			echo json_encode(['status'=>403, 'message'=>'Please enter work performed']); 	
			exit();
		}

		if(empty($note)){
			echo json_encode(['status'=>403, 'message'=>'Please enter note']); 	
			exit();
		}

		$this->load->library('upload');
		if($_FILES['image']['name'] != '')
			{
		$config = array(
			'upload_path' 	=> 'uploads/project_inspection',
			'file_name' 	=> uniqid(),
			'allowed_types' => 'jpg|jpeg|png|gif|webp',
			'max_size' 		=> '10000000',
		);
			$this->upload->initialize($config);
		if ( ! $this->upload->do_upload('image'))
			{
				$error = $this->upload->display_errors();
				echo json_encode(['status'=>403, 'message'=>$error]);
				exit();
			}
			else
			{
			$type = explode('.',$_FILES['image']['name']);
			$type = $type[count($type) - 1];
			$image = 'uploads/project_inspection/'.$config['file_name'].'.'.$type;
			}
		}else{
			echo json_encode(['status'=>403, 'message'=>'Please upload inspection image']); 	
			exit();
		}
	

		if(empty($inspector_name)){
			echo json_encode(['status'=>403, 'message'=>'Please enter inspector name']); 	
			exit();
		}

		$data = array(
			'userID'          => $this->session->userdata('id'),
			'projectID'       => $projectID,
			'location'        => $location,
			'chainage'        => $chainage,
			'inspection_date' => $inspection_date,
			'inspection_time' => $inspection_time,
			'work_perform'    => $work_perform,
			'note'            => $note,
			'image'           => $image,
			'inspector_name'  => $inspector_name,
		);

		$store = $this->project_model->store_road_inspection_maintenance($data);

		if($store){
			echo json_encode(['status'=>200, 'message'=>'Road inspection created successfully']); 	
			exit();
		}else{
			echo json_encode(['status'=>403, 'message'=>'Something went wrong']); 	
			exit();
		}
	}

	public function edit_road_inspection_maintenance(){
		$menuID = 6;
		$permission = $this->permissions()->$menuID;
		if($permission[2]=='Edit'){
    $data['page_title'] = 'Edit Road Inspection Maintenance';
		$id = base64_decode($this->uri->segment(2));
		$data['inspection']=$this->project_model->get_road_inspection_maintenance(array('road_inspetion_maintenance.id'=>$id));
		$data['projects']=$this->project_model->get_projects(array('status'=>1));
		$data['observations']=$this->project_model->get_observations(array('status'=>1));
	  $this->admin_template('road_inspection_maintenance/edit',$data);
		}else{
			redirect(base_url('dashboard'));
		}
	}

	public function view_road_inspection_maintenance(){
		$menuID = 6;
		$permission = $this->permissions()->$menuID;
		if($permission[0]=='View'){
    $data['page_title'] = 'View Road Inspection Maintenance';
		$id = base64_decode($this->uri->segment(2));
		$data['inspection']=$this->project_model->get_road_inspection_maintenance(array('road_inspetion_maintenance.id'=>$id));
		$data['projects']=$this->project_model->get_projects(array('status'=>1));
		$data['observations']=$this->project_model->get_observations(array('status'=>1));
	  $this->admin_template('road_inspection_maintenance/view',$data);
	}else{
		redirect(base_url('dashboard'));
	}
	}
  

	public function update_road_inspection_maintenance(){
		$id = $this->input->post('id');
		$projectID = $this->input->post('projectID');
		$location = $this->input->post('location');
		$chainage = $this->input->post('chainage');
		$inspection_date = $this->input->post('inspection_date');
		$inspection_time = $this->input->post('inspection_time');
		$work_perform = $this->input->post('work_perform');
		$note = $this->input->post('note');
		$inspector_name = $this->input->post('inspector_name');
		$inspection =$this->project_model->get_road_inspection_maintenance(array('road_inspetion_maintenance.id'=>$id));
		if(empty($projectID)){
			echo json_encode(['status'=>403, 'message'=>'Please select project']); 	
			exit();
		}

		if(empty($location)){
			echo json_encode(['status'=>403, 'message'=>'Please enter location']); 	
			exit();
		}

		if(empty($chainage)){
			echo json_encode(['status'=>403, 'message'=>'Please select chainage']); 	
			exit();
		}

		if(empty($inspection_date)){
			echo json_encode(['status'=>403, 'message'=>'Please enter inspection date']); 	
			exit();
		}

		if(empty($inspection_time)){
			echo json_encode(['status'=>403, 'message'=>'Please enter inspection time']); 	
			exit();
		}
		if(empty($work_perform)){
			echo json_encode(['status'=>403, 'message'=>'Please enter work performed']); 	
			exit();
		}

		if(empty($note)){
			echo json_encode(['status'=>403, 'message'=>'Please enter note']); 	
			exit();
		}

		$this->load->library('upload');
		if($_FILES['image']['name'] != '')
			{
		$config = array(
			'upload_path' 	=> 'uploads/project_inspection',
			'file_name' 	=> uniqid(),
			'allowed_types' => 'jpg|jpeg|png|gif|webp',
			'max_size' 		=> '10000000',
		);
			$this->upload->initialize($config);
		if ( ! $this->upload->do_upload('image'))
			{
				$error = $this->upload->display_errors();
				echo json_encode(['status'=>403, 'message'=>$error]);
				exit();
			}
			else
			{
			$type = explode('.',$_FILES['image']['name']);
			$type = $type[count($type) - 1];
			$image = 'uploads/project_inspection/'.$config['file_name'].'.'.$type;
			}
		}else{
			if(!empty($inspection->image)){
				$image = $inspection->image;
			}else{
				echo json_encode(['status'=>403, 'message'=>'Please upload inspection image']); 	
				exit();
			}
			
		}
		if(empty($inspector_name)){
			echo json_encode(['status'=>403, 'message'=>'Please enter inspector name']); 	
			exit();
		}

		$data = array(
			'projectID'       => $projectID,
			'location'        => $location,
			'chainage'        => $chainage,
			'inspection_date' => $inspection_date,
			'inspection_time' => $inspection_time,
			'work_perform'    => $work_perform,
			'note'            => $note,
			'image'           => $image,
			'inspector_name'  => $inspector_name,
		);

		$update = $this->project_model->update_road_inspection_maintenance($data,array('id'=>$id));

		if($update){
			echo json_encode(['status'=>200, 'message'=>'Road inspection updated successfully']); 	
			exit();
		}else{
			echo json_encode(['status'=>403, 'message'=>'Something went wrong']); 	
			exit();
		}
	}

  public function delete_road_inspection_maintenance(){
		$id = $this->input->post('id');
		$delete = $this->project_model->delete_road_inspection_maintenance($id);
		if($delete){
			echo json_encode(['status'=>200, 'message'=>'Road inspection deleted successfully']); 	
			exit();
		}else{
			echo json_encode(['status'=>403, 'message'=>'Something went wrong']); 	
			exit();
		}
	}

	// public function export_project(){
	// 	$this->load->library('excel');
	
	// 		$object = new PHPExcel();
	// 		$object->setActiveSheetIndex(0);
	// 		$table_columns = array("S.No", "Project Name", "Location", "Chainage", "Inspection Date", "Inspection Time", "Observation", "Note", "Image");
	
	// 		$column = 0;
	
	// 		foreach($table_columns as $field) {
	// 				$object->getActiveSheet()->setCellValueByColumnAndRow($column, 1, $field);
	// 				$column++;
	// 		}
			
	// 		$projects = $this->project_model->get_road_inspection_maintenances(array('road_inspetion_maintenance.status'=>1));;
	// 		//$expence_data = $this->Project_model->get_expences(array('status'=>1));
	// 		//print_r($expence_data);die;
			
	// 		$excel_row = 2;
	// 		$count = 1;
	// 		foreach($projects as $project) {
	// 			$image = '<img src="'.base_url($project->image).'" >';
	// 			$object->getActiveSheet()->setCellValueByColumnAndRow(0, $excel_row, $count++);
	// 			$object->getActiveSheet()->setCellValueByColumnAndRow(1, $excel_row, $project->projectName);
	// 			$object->getActiveSheet()->setCellValueByColumnAndRow(2, $excel_row, $project->location);
	// 			$object->getActiveSheet()->setCellValueByColumnAndRow(3, $excel_row, $project->chainageName);
	// 			$object->getActiveSheet()->setCellValueByColumnAndRow(4, $excel_row, date('d-m-Y',strtotime($project->inspection_date)));
	// 		  $object->getActiveSheet()->setCellValueByColumnAndRow(5, $excel_row, date('H:i A',strtotime($project->inspection_time)));
	// 		  $object->getActiveSheet()->setCellValueByColumnAndRow(6, $excel_row, $project->observationName);
	// 		  $object->getActiveSheet()->setCellValueByColumnAndRow(7, $excel_row, $project->note);
	// 		  $object->getActiveSheet()->setCellValueByColumnAndRow(8, $excel_row, $image);
	// 				$excel_row++;
	// 		}
	// 		$this_date = "road_inspection_maintenance".date("YmdHis");
	// 		$filename= $this_date.'.csv'; //save our workbook as this file name
	// 		header('Content-Type: application/vnd.ms-excel; charset=UTF-8'); //mime type
	// 		header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
	// 		header('Cache-Control: max-age=0'); //no cache
	// 		header('Content-Transfer-Encoding: BINARY'); //
	
	// 		$objWriter = PHPExcel_IOFactory::createWriter($object, 'CSV');
	// 		//ob_end_clean();
	// 		$objWriter->save('php://output');
	
	//  }

	public function export_project(){
			$output = "";
      $project_name = $this->project_model->get_project(array('id'=>$this->session->userdata('project')));
			$output .= '
			
			<table style="border: 1px solid black" class="table-striped">
			         <thead style="border: 1px solid black">
							 <tr style="border: 1px solid black">
							  <th>Sr No.</th>
								<th>Project Name</th>
								<th>Chainage</th>
								<th>Latitude</th>
								<th>Longitude</th>
								<th>Inspection Date</th>
								<th>Inspection Time</th>
								<th>Defect</th>
								<th>Note</th>
								<th>Inspected By</th>
								<th style="width:140px">Image</th>
								</tr>
							 </thead><tbody style="border: 1px solid black">';
							 $projects = $this->project_model->get_road_inspection_maintenances(array('road_inspetion_maintenance.status'=>1));
							// print_r($projects);
              
							 foreach($projects as $key=>$project) {
								$image = '<img src="'.base_url($project->image).'" width="100" height="85" style="margin-top:5px">';
								$location = explode("/", $project->location);
								$son = $key+1;
								$output .= '<tr style="border: 1px solid black; background-color:#eee">
										<td style="height:110px; text-align:center;vertical-align: middle;">'.$son.'</td>
										<td style="height:110px; text-align:center;vertical-align: middle;">'.$project->projectName.'</td>
										<td style="height:110px; text-align:center;vertical-align: middle;">'.$project->chainage.'</td>
										<td style="height:110px; text-align:center;vertical-align: middle;">'.$location[0].'</td>
										<td style="height:110px; text-align:center;vertical-align: middle;">'.$location[1].'</td>
										<td style="height:110px; text-align:center;vertical-align: middle;">'.date('d-m-Y',strtotime($project->inspection_date)).'</td>
										<td style="height:110px; text-align:center;vertical-align: middle;">'.date('H:i A',strtotime($project->inspection_time)).'</td>
										<td style="height:110px; text-align:center;vertical-align: middle;">'.$project->workPerformName.'</td>
										<td style="height:110px; text-align:center;vertical-align: middle;">'.$project->note.'</td>
										<td style="height:110px; text-align:center;vertical-align: middle;">'.$project->inspector_name.'</td>
										<td style="height:90px;width:90px; text-align:center;vertical-align: middle;">'.$image.'</td></tr>
								';
							 }
			
			$output .='</tbody></table>';
			$filename = $project_name->name.'-'.date("Y-m-d");
			header('Content-Type: application/force-download'); //mime type
			header('Content-Disposition: attachment;filename="'.$filename.'.xls"'); //tell browser what's the file name
			header('Content-Transfer-Encoding: BINARY'); //
   
			echo $output;
	
	 }
	
}