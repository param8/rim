<?php 
class MY_Controller extends CI_Controller 
{
	var $permission = array();

	public function __construct() 
	{
	   parent::__construct();
 
    $this->load->model('setting_model');
		$this->load->model('user_model');
		$this->load->helper('mail');
		$this->load->helper('sms_helper');
		if(empty($this->session->userdata('logged_in'))) {
			$session_data = array('logged_in' => FALSE);
			$this->session->set_userdata($session_data);
		}
	}

	public function logged_in()
	{
		$session_data = $this->session->userdata();
		
		if($session_data['logged_in'] == TRUE) {
			if($session_data['user_type'] == '1' || $session_data['user_type']=='2' || $session_data['user_type']=='3' || $session_data['user_type']=='4')
			{
				redirect('dashboard', 'refresh');	
			}
		}
	}


	public function not_admin_logged_in()
	{
	
		$session_data = $this->session->userdata();
		if($session_data['logged_in'] == FALSE && ($session_data['user_type'] == 1 || $session_data['user_type'] == 2 || $session_data['user_type'] == 3 || $session_data['user_type'] == 4)) {
			redirect('Authantication', 'refresh');
		}
	}

	public function siteinfo(){
		$siteinfo = $this->setting_model->get_site_info();
	  return $siteinfo;
    
	 }

	 public function menus(){
		$menus = $this->setting_model->get_menus(array('status'=>1));
	  return $menus;
    
	 }

	 public function roles(){
		$roles = $this->setting_model->get_roles(array('status'=>1));
	  return $roles; 
	 }

	 public function permissions(){
		$session_data = $this->session->userdata();
		$permissions = array();
	 if($session_data['logged_in'] == TRUE){
		$user = $this->user_model->get_user(array('users.id'=>$session_data['id']));
		$permissions = json_decode($user->permission);
	 }
	  return $permissions; 
	 }

	 public function user_role(){
		$session_data = $this->session->userdata();
	 if($session_data['logged_in'] == TRUE){
		$role = $this->setting_model->get_role(array('status'=>1,'id'=>$session_data['user_type']));
		$user_role = $role->name;
	 }
	  return $user_role; 
	 }

	 public function login_template($page = null, $data = array())
	 {
		$data['siteinfo'] = $this->siteinfo();
		$this->load->view('components/head',$data);
		$this->load->view($page);
		$this->load->view('components/footer');
	 }





	 public function admin_template($page = null, $data = array())
	 {
		$data['siteinfo'] = $this->siteinfo();
		$data['menus'] = $this->menus();
		$data['roles'] = $this->roles();
		$data['permissions'] = $this->permissions();
		$data['user_role'] = $this->user_role();
		$this->load->view('components/head',$data);
		$this->load->view('components/header');
		$this->load->view('components/sidebar');
		$this->load->view($page);
		$this->load->view('components/footer');
	 }


}