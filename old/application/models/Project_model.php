<?php 

class Project_model extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
	}

	function make_query($condition)
  {
    $this->db->select('project.*,users.name as userName,users.phone as userPhone,users.address as userAddress');
    $this->db->from('project');
    $this->db->join('users','users.id=project.clientID','left');
    $this->db->where($condition);

   if(isset($_POST["search"]["value"]) && !empty($_POST["search"]["value"]))
   {
    $this->db->group_start();
    $this->db->like('project.name', $_POST["search"]["value"]);
    $this->db->group_end();
   }
   $this->db->order_by('project.id','desc');
    
  }
    function make_datatables($condition){
	  $this->make_query($condition);
	  if($_POST["length"] != -1)
	  {
		  $this->db->limit($_POST['length'], $_POST['start']);
	  }
	  $query = $this->db->get();
	  return $query->result_array(); 
  // echo $this->db->last_query(); die;
  }

  function get_filtered_data($condition){
	  $this->make_query($condition);
	  $query = $this->db->get();
	  return $query->num_rows();
	  //echo $this->db->last_query();die;
  }
  function get_all_data($condition)
  {
    $this->db->select('project.*,users.name as userName,users.phone as userPhone,users.address as userAddress');
    $this->db->from('project');
    $this->db->join('users','users.id=project.clientID','left');
    $this->db->where($condition);

   if(isset($_POST["search"]["value"]) && !empty($_POST["search"]["value"]))
   {
    $this->db->group_start();
    $this->db->like('project.name', $_POST["search"]["value"]);
    $this->db->group_end();
   }
   $this->db->order_by('project.id','desc');
	   return $this->db->count_all_results();
  }

  public function get_project($condition){
    $this->db->select('project.*');
    $this->db->from('project');
    $this->db->where($condition);
	  return $this->db->get()->row();
  }
  public function get_projects($condition){
    $this->db->select('project.*');
    $this->db->from('project');
    $this->db->where($condition);
	  return $this->db->get()->result();
  }

  public function store_project($data){
	 $this->db->insert('project',$data);
   return $this->db->insert_id();
  }

  public function update_project($data,$id){
	 $this->db->where('id',$id);
	 return $this->db->update('project',$data);
  }

  public function store_chainage($data){
   return $this->db->insert('chainage',$data);
  }

  public function get_chainages($condition){
    $this->db->select('chainage.*');
    $this->db->from('chainage');
    $this->db->where($condition);
	  return $this->db->get()->result();
  }

  public function update_chainage($data,$id){
    $this->db->where('id',$id);
	 return $this->db->update('chainage',$data);
  }

  public function delete_chainage($id){
		$this->db->where('projectID', $id);
		return $this->db->delete('chainage');
	}


  // Observation

  function make_query_observation($condition)
  {
    $this->db->select('work_perform.*');
    $this->db->from('work_perform');
    $this->db->where($condition);

   if(isset($_POST["search"]["value"]) && !empty($_POST["search"]["value"]))
   {
    $this->db->group_start();
    $this->db->like('work_perform.name', $_POST["search"]["value"]);
    $this->db->group_end();
   }
   $this->db->order_by('work_perform.id','desc');
    
  }
    function make_datatables_observation($condition){
	  $this->make_query_observation($condition);
	  if($_POST["length"] != -1)
	  {
		  $this->db->limit($_POST['length'], $_POST['start']);
	  }
	  $query = $this->db->get();
	  return $query->result_array(); 
  // echo $this->db->last_query(); die;
  }

  function get_filtered_data_observation($condition){
	  $this->make_query_observation($condition);
	  $query = $this->db->get();
	  return $query->num_rows();
	  //echo $this->db->last_query();die;
  }
  function get_all_data_observation($condition)
  {
    $this->db->select('work_perform.*');
    $this->db->from('work_perform');
    $this->db->where($condition);

   if(isset($_POST["search"]["value"]) && !empty($_POST["search"]["value"]))
   {
    $this->db->group_start();
    $this->db->like('work_perform.name', $_POST["search"]["value"]);
    $this->db->group_end();
   }
   $this->db->order_by('work_perform.id','desc');
	   return $this->db->count_all_results();
  }


  public function get_observation($condition){
    $this->db->select('work_perform.*');
    $this->db->from('work_perform');
    $this->db->where($condition);
	  return $this->db->get()->row();
  }
  public function get_observations($condition){
    $this->db->select('work_perform.*');
    $this->db->from('work_perform');
    $this->db->where($condition);
	  return $this->db->get()->result();
  }

  public function store_observation($data){
	 $this->db->insert('work_perform',$data);
   return $this->db->insert_id();
  }

  public function update_observation($data,$id){
	 $this->db->where('id',$id);
	 return $this->db->update('work_perform',$data);
  }


  // Road Inspection Maintenance

  function make_query_road_inspection_maintenance($condition)
  {
    $this->db->select('road_inspetion_maintenance.*,project.name as projectName,work_perform.name as workPerformName,users.name as userName ');
    $this->db->from('road_inspetion_maintenance');
    $this->db->join('project','project.id = road_inspetion_maintenance.projectID','left');
    $this->db->join('work_perform','work_perform.id = road_inspetion_maintenance.work_perform','left');
    $this->db->join('users','users.id = road_inspetion_maintenance.userID','left');
    $this->db->where($condition);


    if($this->session->userdata('start_date') && $this->session->userdata('end_date')){
      $start_date = date('Y-m-d',strtotime($this->session->userdata('start_date')));
      $end_date = date('Y-m-d',strtotime($this->session->userdata('end_date')));
      $this->db->where("DATE_FORMAT(road_inspetion_maintenance.inspection_date,'%Y-%m-%d') >='$start_date'");
      $this->db->where("DATE_FORMAT(road_inspetion_maintenance.inspection_date,'%Y-%m-%d') <='$end_date'");
      //$this->db->where("road_inspetion_maintenance.inspection_date BETWEEN '$start_date' AND '$end_date'");
    }

   if(isset($_POST["search"]["value"]) && !empty($_POST["search"]["value"]))
   {
    $this->db->group_start();
    $this->db->or_like('project.name', $_POST["search"]["value"]);
    $this->db->or_like('work_perform.name', $_POST["search"]["value"]);
    $this->db->or_like('users.name', $_POST["search"]["value"]);
    $this->db->or_like('road_inspetion_maintenance.chainage', $_POST["search"]["value"]);
    $this->db->group_end();
   }
   $this->db->order_by('road_inspetion_maintenance.id','desc');
    
  }
    function make_datatables_road_inspection_maintenance($condition){
	  $this->make_query_road_inspection_maintenance($condition);
	  if($_POST["length"] != -1)
	  {
		  $this->db->limit($_POST['length'], $_POST['start']);
	  }
	  $query = $this->db->get();
	  return $query->result_array(); 
   //echo $this->db->last_query(); die;
  }

  function get_filtered_data_road_inspection_maintenance($condition){
	  $this->make_query_road_inspection_maintenance($condition);
	  $query = $this->db->get();
	  return $query->num_rows();
	  //echo $this->db->last_query();die;
  }
  function get_all_data_road_inspection_maintenance($condition)
  {
    $this->db->select('road_inspetion_maintenance.*,project.name as projectName,work_perform.name as workPerformName,users.name as userName ');
    $this->db->from('road_inspetion_maintenance');
    $this->db->join('project','project.id = road_inspetion_maintenance.projectID','left');
    $this->db->join('work_perform','work_perform.id = road_inspetion_maintenance.work_perform','left');
    $this->db->join('users','users.id = road_inspetion_maintenance.userID','left');
    $this->db->where($condition);
    if($this->session->userdata('location')){
      $this->db->where('road_inspetion_maintenance.location', $this->session->userdata('location'));
    }

    if($this->session->userdata('start_date') && $this->session->userdata('end_date')){
      $start_date = date('Y-m-d',strtotime($this->session->userdata('start_date')));
      $end_date = date('Y-m-d',strtotime($this->session->userdata('end_date')));
      $this->db->where("DATE_FORMAT(road_inspetion_maintenance.inspection_date,'%Y-%m-%d') >='$start_date'");
      $this->db->where("DATE_FORMAT(road_inspetion_maintenance.inspection_date,'%Y-%m-%d') <='$end_date'");
      //$this->db->where("road_inspetion_maintenance.inspection_date BETWEEN '$start_date' AND '$end_date'");
    }

   if(isset($_POST["search"]["value"]) && !empty($_POST["search"]["value"]))
   {
    $this->db->group_start();
    $this->db->or_like('project.name', $_POST["search"]["value"]);
    $this->db->or_like('work_perform.name', $_POST["search"]["value"]);
    $this->db->or_like('users.name', $_POST["search"]["value"]);
    $this->db->or_like('road_inspetion_maintenance.chainage', $_POST["search"]["value"]);
    $this->db->group_end();
   }
   $this->db->order_by('road_inspetion_maintenance.id','desc');
	   return $this->db->count_all_results();
  }


  public function get_road_inspection_maintenance($condition){
    $this->db->select('road_inspetion_maintenance.*,project.name as projectName,work_perform.name as workPerformName,users.name as userName ');
    $this->db->from('road_inspetion_maintenance');
    $this->db->join('project','project.id = road_inspetion_maintenance.projectID','left');
    $this->db->join('work_perform','work_perform.id = road_inspetion_maintenance.work_perform','left');
    $this->db->join('users','users.id = road_inspetion_maintenance.userID','left');
    $this->db->where($condition);
	  return $this->db->get()->row();
  }
  public function get_road_inspection_maintenances($condition){
    $this->db->select('road_inspetion_maintenance.*,project.name as projectName,work_perform.name as workPerformName,users.name as userName ');
    $this->db->from('road_inspetion_maintenance');
    $this->db->join('project','project.id = road_inspetion_maintenance.projectID','left');
    $this->db->join('work_perform','work_perform.id = road_inspetion_maintenance.work_perform','left');
    $this->db->join('users','users.id = road_inspetion_maintenance.userID','left');
    $this->db->where($condition);
    if($this->session->userdata('project')){
      $this->db->where('road_inspetion_maintenance.projectID', $this->session->userdata('project'));
    }

    if($this->session->userdata('start_date') && $this->session->userdata('end_date')){
      $start_date = date('Y-m-d',strtotime($this->session->userdata('start_date')));
      $end_date = date('Y-m-d',strtotime($this->session->userdata('end_date')));
      $this->db->where("DATE_FORMAT(road_inspetion_maintenance.inspection_date,'%Y-%m-%d') >='$start_date'");
      $this->db->where("DATE_FORMAT(road_inspetion_maintenance.inspection_date,'%Y-%m-%d') <='$end_date'");
     // $this->db->where("road_inspetion_maintenance.inspection_date BETWEEN '$start_date' AND '$end_date'");
    }
	  return $this->db->get()->result();
  }

  public function get_road_inspections($condition){
    $this->db->select('road_inspetion_maintenance.*,project.name as projectName,work_perform.name as workPerformName,users.name as userName ');
    $this->db->from('road_inspetion_maintenance');
    $this->db->join('project','project.id = road_inspetion_maintenance.projectID','left');
    $this->db->join('work_perform','work_perform.id = road_inspetion_maintenance.work_perform','left');
    $this->db->join('users','users.id = road_inspetion_maintenance.userID','left');
    $this->db->where($condition);
	  return $this->db->get()->result();
  }


  public function store_road_inspection_maintenance($data){
	 $this->db->insert('road_inspetion_maintenance',$data);
   return $this->db->insert_id();
  }

  public function update_road_inspection_maintenance($data,$condition){
	 $this->db->where($condition);
	 return $this->db->update('road_inspetion_maintenance',$data);
  }

  public function delete_road_inspection_maintenance($id){
    $this->db->where('id',$id);
    return $this->db->delete('road_inspetion_maintenance');
  }

  public function store_road_inspection_history($data){
    $this->db->insert('road_inspetion_history',$data);
    return $this->db->insert_id();
   }


}